'use strict';
module.exports = (sequelize, DataTypes) => {
  var Card = sequelize.define('Card', {
    img: DataTypes.TEXT,
    link: DataTypes.TEXT,
    title_ru: DataTypes.STRING,
    title_en: DataTypes.STRING,
    is_news: DataTypes.BOOLEAN,
    tags: DataTypes.STRING,
    text_ru: DataTypes.TEXT,
    text_en: DataTypes.TEXT,
    news_date: DataTypes.DATE
  }, {});
  Card.associate = function(models) {
    // associations can be defined here
  };
  return Card;
};