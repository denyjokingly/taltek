import React, { Component } from "react";
import { Form, Radio, Input, Row, Col, Button, Tabs, Calendar } from "antd";
import { Controlled as CodeMirror } from "react-codemirror2";
import "codemirror/mode/xml/xml";
import { message } from "antd/lib/index";
import moment from "moment";

const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const success = () => {
  message.success("Изменения успешно применены");
};
const CustomForm = Form.create({
  onFieldsChange(props, changedFields) {
    props.onChange(changedFields);
  },
  mapPropsToFields(props) {
    return {
      img: Form.createFormField({
        ...props.img,
        value: props.img.value
      }),
      title_ru: Form.createFormField({
        ...props.title_ru,
        value: props.title_ru.value
      }),
      title_en: Form.createFormField({
        ...props.title_en,
        value: props.title_en.value
      })
    };
  },
  onValuesChange(_, values) {
    //console.log(values);
  }
})(props => {
  const { getFieldDecorator } = props.form;
  const { text_ru, text_en } = props;
  const disabled = props.img.value.search("http") == -1 ? true : false;
  return (
    <Row>
      <Col>
        <Form onSubmit={props.onSubmit}>
          <FormItem label="Дата новости">
            <Calendar
              value={props.news_date}
              style={{ width: 300 }}
              onChange={props.dateChanged}
              onPanelChange={props.handlePanelChange}
              fullscreen={false}
            />
          </FormItem>
          <FormItem
            style={{ display: props.type === "video" ? "none" : "block" }}
            label="Ссылка на картинку"
          >
            {getFieldDecorator("img", {
              rules: [
                { required: false, message: "Поле не должно быть пустым" }
              ]
            })(
              <div style={{ display: "none", justifyContent: "center" }}>
                <Input
                  defaultValue={props.type === "video" ? "" : props.img.value}
                />
                <Button
                  icon="cloud-download"
                  style={{ marginLeft: 50 }}
                  disabled={disabled}
                  onClick={props.saveExtToLocal.bind(null, props.img)}
                >
                  Сохранить
                </Button>
              </div>
            )}
          </FormItem>
          <FormItem label="Заголовок (русский)">
            {getFieldDecorator("title_ru", {
              rules: [{ required: true, message: "Поле не должно быть пустым" }]
            })(<Input />)}
          </FormItem>
          <FormItem label="Заголовок (английский)">
            {getFieldDecorator("title_en", {
              rules: [{ required: true, message: "Поле не должно быть пустым" }]
            })(<Input />)}
          </FormItem>
          <FormItem label="Текст новости">
            <Tabs defaultActiveKey="1">
              <Tabs.TabPane tab="русский" key="1">
                <CodeMirror
                  value={text_ru}
                  className="codemirror-editor"
                  options={{
                    mode: "xml",
                    theme: "default",
                    lineNumbers: true,
                    lineWrapping: true
                  }}
                  onBeforeChange={(editor, data, value) => {
                    props.handleText(value, "ru");
                  }}
                  onChange={(editor, data, value) => {
                    props.handleText(value, "ru");
                  }}
                />
              </Tabs.TabPane>
              <Tabs.TabPane tab="английский" key="2">
                <CodeMirror
                  value={text_en}
                  className="codemirror-editor"
                  options={{
                    mode: "xml",
                    theme: "default",
                    lineNumbers: true,
                    lineWrapping: true
                  }}
                  onBeforeChange={(editor, data, value) => {
                    props.handleText(value, "en");
                  }}
                  onChange={(editor, data, value) => {
                    props.handleText(value, "en");
                  }}
                />
              </Tabs.TabPane>
            </Tabs>
          </FormItem>
          <Button
            type="primary"
            disabled={
              props.news_date == "" ||
              props.title_ru == "" ||
              props.text_ru == "" ||
              props.img == ""
            }
            htmlType="submit"
          >
            Сохранить
          </Button>
        </Form>
      </Col>
    </Row>
  );
});

export default class News extends Component {
  state = {
    type: "news",
    locked: true,
    isNew: true,
    text_ru: "",
    text_en: "",
    news_date: moment(),
    fields: {
      img: {
        value: ""
      },
      title_ru: {
        value: ""
      },
      title_en: {
        value: ""
      }
    }
  };

  componentDidMount() {
    this.getCard();
  }

  getCard() {
    const {
      match: { params }
    } = this.props;
    if (!params.pageId) {
      this.setState({ locked: false });
      return;
    }
    fetch(`/api/card/${params.pageId}`)
      .then(res => res.json())
      .then(card => this.patchFields(card));
  }

  onChange = e => {
    this.setState({
      type: e.target.value
    });
  };

  saveExtToLocal = img => {
    fetch("/api/upload/asset", {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify({ link: img.value })
    })
      .then(res => res.json())
      .then(({ file }) => {
        const { fields } = this.state;

        fields.img.value = file;

        this.setState({ fields: fields });
        this.handleAddNews();
        window.location.reload();
      });
  };

  patchFields(card) {
    const updatedFields = Object.assign({}, this.state.fields);
    for (let prop in card) {
      if (updatedFields[prop]) updatedFields[prop].value = card[prop];
    }
    this.setState(state => ({
      fields: updatedFields,
      isNew: false,
      news_date: moment(card.news_date),
      text_ru: card.text_ru,
      text_en: card.text_en,
      locked: false,
      type: card.tags
    }));
    this.render();
  }

  handleAddNews = e => {
    if (e) {
      e.persist();
      e.preventDefault();
    }

    const article = {
      img: this.state.fields.img.value,
      title_ru: this.state.fields.title_ru.value,
      title_en: this.state.fields.title_en.value,
      text_ru: this.state.text_ru,
      text_en: this.state.text_en,
      news_date: this.state.news_date.format(),
      tags: this.state.type,
      is_news: true,
      link: ""
    };
    const {
      match: { params }
    } = this.props;
    const route = this.state.isNew ? "" : `${params.pageId}`;

    fetch(`/api/card/${route}`, {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(article)
    })
      .then(res => res.json())
      .then(() => {
        success();
        this.getCard();
      });
  };

  handleText = (value, locale) => {
    this.setState({ ["text_" + locale]: value });
  };

  handleFormChange = changedFields => {
    this.setState(({ fields }) => ({
      fields: { ...fields, ...changedFields }
    }));
  };

  dateChanged = value => {
    console.log("date changed", value);
    this.setState({ news_date: value });
  };

  handlePanelChange = value => {
    console.log("panel changed", value);
    this.setState({ news_date: value });
  };

  render() {
    const { fields } = this.state;
    if (this.state.locked) return null;

    return (
      <div style={{ maxWidth: 960, margin: "auto" }}>
        <h2 style={{ padding: 10 }}>Добавить :</h2>
        <RadioGroup
          onChange={this.onChange}
          value={this.state.type}
          style={{ justifyContent: "center", display: "none" }}
        >
          <Radio.Button value="news">Новость</Radio.Button>
          <Radio.Button value="video">Видео</Radio.Button>
          <Radio.Button value="pr">Пресс-релиз</Radio.Button>
        </RadioGroup>
        <CustomForm
          {...fields}
          saveExtToLocal={this.saveExtToLocal}
          onSubmit={this.handleAddNews}
          isNew={this.state.isNew}
          type={this.state.type}
          news_date={this.state.news_date}
          handlePanelChange={this.handlePanelChange}
          dateChanged={this.dateChanged}
          onChange={this.handleFormChange}
          handleText={this.handleText}
          text_ru={this.state.text_ru}
          text_en={this.state.text_en}
        />
      </div>
    );
  }
}
