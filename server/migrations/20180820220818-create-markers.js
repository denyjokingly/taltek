'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Markers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title_ru: {
        type: Sequelize.STRING
      },
      title_en: {
        type: Sequelize.STRING
      },
      text_ru: {
        type: Sequelize.TEXT
      },
      text_en: {
        type: Sequelize.TEXT
      },
      link: {
        type: Sequelize.TEXT
      },
      className: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Markers');
  }
};