const Card = require('../models').Card;

module.exports = {
    create(req, res) {
        return Card
            .create({
                img: req.body.img,
                link: req.body.link,
                title_ru: req.body.title_ru,
                title_en: req.body.title_en,
                is_news: req.body.is_news,
                tags: req.body.tags,
                text_en: req.body.text_en,
                text_ru: req.body.text_ru,
                news_date: req.body.news_date,
            })
            .then( card => res.status(200).send(card))
            .catch( err => res.status(400).send(err));
    },
    list(req, res) {
        return Card
            .findAll()
            .then( cards => res.status(200).send(cards))
    },
    retrieve(req, res) {
        return Card
            .findById(req.params.cardId)
            .then( card => {
                if(!card) {
                    return res.status(404).send({
                        message: "страница не найдена"
                    });
                }
                return res.status(200).send(card);
            })
            .catch( err => res.status(400).send(err));
    },
    update(req, res) {
        return Card
            .findById(req.params.cardId)
            .then( card => {
                if(!card){
                    console.log('Page not found!');
                    return res.status(404).send({
                        message: 'todo not found'
                    });
                }

                return card
                    .update( req.body, {
                        fields: Object.keys(req.body)
                    })
                    .then(()=> res.status(200).send(card))
                    .catch( err => res.status(400).send(err));
            })
            .catch( err => res.status(400).send(err));
    },
    destroy(req, res) {
        return Card
            .findById(req.params.cardId)
            .then( card => {
                if(!card) {
                    return res.status(400).send({message: 'Страница не найдена' });
                }
                return card
                    .destroy()
                    .then( () => res.status(204).send())
                    .catch( err => res.status(400).send(err));
            })
            .catch( err => res.status(400).send(err));
    }
};