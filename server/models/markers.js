'use strict';
module.exports = (sequelize, DataTypes) => {
  var Markers = sequelize.define('Markers', {
    title_ru: DataTypes.STRING,
    title_en: DataTypes.STRING,
    text_ru: DataTypes.TEXT,
    text_en: DataTypes.TEXT,
    link: DataTypes.TEXT,
    className: DataTypes.TEXT
  }, {});
  Markers.associate = function(models) {
    // associations can be defined here
  };
  return Markers;
};