import React, { Component } from 'react';
import { Row, Col } from 'antd';
import {Link} from 'react-router-dom';
import arrow from '../images/arrows-link.svg';


export default class Cards extends Component {

    render() {
        return (
            <div className="main-page-content">
                <Row>
                    {this.props.pool && this.props.pool.map( (x,i) => {
                        return (
                            <Col key={i} className="card" style={{ backgroundImage: `url(${x.img})`, backgroundSize: "cover"}} span={ i === this.props.pool.length - 1 ? 16 : 8}>
                                <h1>{x['title_'+this.props.locale]}</h1>
                                <Link to={x.link} dangerouslySetInnerHTML={{__html: this.props.locale === 'ru' ? "Подробнее    &xrarr;":"Learn more    &xrarr;"}}></Link>
                            </Col>
                        )
                    })}
                </Row>
            </div>
        );
    }
}

