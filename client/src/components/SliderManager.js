import React, {Component} from 'react';
import { Divider, List, Icon, Popconfirm} from 'antd';
import {Link} from 'react-router-dom';

export default class SliderManager extends Component {
    state = {
        sliders:[],
    };
    componentDidMount(){
        this.getSliders();
    }

    getSliders(){
        fetch('/api/slider')
            .then( res => res.json())
            .then( sliders => this.setState({sliders: sliders}))
            .catch( err => console.log(err));
    }

    formChange = (e, slider, slide) => {
        // console.log(e, slider, slide);
    };

    deleteSlider(id) {
        fetch('/api/slider/'+id, {
            method: 'DELETE'
        })
            .then( () => this.getSliders())
            .catch( err => console.log(err));

    }

    render(){
        const {sliders} = this.state;
        if(!sliders) return null;
        console.log(sliders);

        return (
        <div className="pages-list-wrapper">
            <h2>Список слайдеров:</h2>
            <Divider />
            <List
                itemLayout="horizontal"
                dataSource={sliders}
                size="small"
                renderItem= { item => (
                    <List.Item actions={[<Link style={{ textDecoration: "none", color: "#a1a1a1"}} to={`/admin/edit/slider/${item.id}`}><Icon type="edit"/></Link>,
                        <Popconfirm title="Подтвердите действие" 
                        okText="Да" 
                        cancelText="Нет" 
                        onCancel={ () => console.log('cancel')} 
                        onConfirm={ this.deleteSlider.bind(this,`${item.id}`) }><a style={{ textDecoration: "none", color: "#a1a1a1"}}><Icon type="close"/></a></Popconfirm> ]}>
                        {item.content.name}
                    </List.Item>
                )}
            />
        </div>
        )
    }
}