import React, { Component } from 'react';
import { Popover, Tooltip, Button    } from 'antd';
import '../App.css';

const markers = [
     {name: "Murm", coordinates: [35.04, 66.8]},
     {name: "SPB", coordinates: [30, 60]},
     {name: "MSK", coordinates: [37.6,55.4]},
    {name: "Altai", coordinates: [84,53]},
    {name:"Kemerovo", coordinates: [88, 55]},
    {name:"Kemerovo", coordinates: [89, 54]},
    {name:"Kemerovo", coordinates: [87, 54]}
];

const cities = [
    "Murmanskaya Oblast",
    "Saint Petersburg|Sankt-Peterburgskaya G.|Leningradskaya Oblast",
    "Mosca|Moscou|Moscow|Moskau|Moskova|Moskovskaya",
    "Altayskiy Kray",
    "Kemerovskaya Oblast"
];

const texts = [
    {
        ru:{
            head:"&laquo;Разрез Поляны&raquo;",
            text:"«Разрез Поляны» и «Разрез Луговое» ведет добычу угля в северо-восточной части киселевского каменноугольного месторождения Прокопьевско-Киселевского района."
        },
        en:{
            head:"",
            text:""
        }
    },
    {
        ru:{
            head: "&laquo;Разрез Талдинский-Западный&raquo;",
            text:"«Разрез Талдинский-Западный» ведет добычу угля на участке «Разрез Тагарышский» Талдинского каменноугольного месторождения."
        },
        en:{
            head:"",
            text:""
        }
    },
    {
        ru:{
            head: "&laquo;Разрез ТалТЭК&raquo;",
            text:"«Разрез ТалТЭК» ведет добычу угля на лицензионном участке «Кыргайский Промежуточный» Талдинского каменноугольного месторождения. "
        },
        en:{
            head:"",
            text:""
        }
    },
    {
        ru:{
            head: "&laquo;ТалТЭК Транс&raquo;",
            text:"«ТалТЭК Транс» обеспечивает транспортно-логистический функционал Группы."
        },
        en:{
            text:"",
            head:"",
        }
    },
    {
        ru:{
            head: "&laquo;Барнаульский&raquo; БРЗ",
            text:"Барнаульский БРЗ изготавливает и ремонтирует железнодорожный грузовой подвижной состав и запасные части. "
        },
        en:{
            head:"",
            text:""
        }
    },
    {
        ru:{
            head: "&laquo;Кандалакшский&raquo; МТП",
            text:"Кандалакшский МТП участвует в перевалке угля ТАЛТЭК на экспорт и обеспечивает грузопоток генеральных грузов в Арктику."
        },
        en:""
    },
    {
        ru:{
            head: "&laquo;ГК ТАЛТЕК&raquo;",
            text:"Центральный офис ГК ТАЛТЭК в Москве"
        },
        en:{
            head:"",
            text:""
        }
    }
];


export default class GeographySection extends Component {

    state = {
        loadMore: true,
        markers: [],
    }

    content = (this.state.markers.map( (marker,idx) => {
        return (
            <div key={idx} className="popover-container">
                <p className="popover-container__text"
                   dangerouslySetInnerHTML={{__html: marker['text_'+this.props.locale]}}/>
            </div>
        )
    }
    ));

    getContent = () => (
        this.state.markers.map( (marker, idx) => (

        <div key={idx} className="popover-container">
            <p className="popover-container__text"
            dangerouslySetInnerHTML={{__html: marker['text_'+this.props.locale]}}/>
        </div>
        ))
    )

    componentDidMount(){
        fetch('/api/marker')
        .then(res => res.json())
        .then( markers => {
            this.setState({markers: markers})
        
        })
        .catch(err=> console.log(err));

    }

    handleClick(geo, evt){
        // console.log(geo);
    }

    getTexts = () =>{
        return this.state.loadMore ?
            this.state.markers.slice(0,3) : this.state.markers;
    };

    showAllCards = () => {
      this.setState({loadMore: false});
    };

    getMarkerPos = idx => {
        let pos = {left: 0, top: 0};
        switch (idx) {
            case 0:
                pos.left = 777;
                pos.top = 770;
                break;
            case 1:
                pos.left = 795;
                pos.top = 775;
                break;
            case 2:
                pos.left = 785;
                pos.top = 755;
                break;
            case 3:
                pos.left = 254;
                pos.top = 375;
                break;
            case 4:
                pos.left = 730;
                pos.top = 820;
                break;
            case 5:
                pos.left = 410;
                pos.top = 355;
                break;
            case 6:
                pos.left = 254;
                pos.top = 520;
                break;
        }
        return pos;
    };

    render() {
        return (
            <div className="main-page-container geo">
                    <div className="minimap main-page-content">
                        <h1 className="minimap-heading" dangerouslySetInnerHTML={{ __html : this.props.locale === 'ru'? "География ГК Талтек" : " GK TALTEK GEOGRAPHY"}}/>
                        <div className="geo-cards">
                            {this.getTexts().map( (marker, idx) => (
                                <div key={idx} className="geo-card">
                                    <h1 className="geo-card-heading" dangerouslySetInnerHTML={{__html:marker['title_'+this.props.locale]}}/>
                                    <p className="geo-card-text" dangerouslySetInnerHTML={{
                                        __html: marker['text_'+this.props.locale]
                                    }}/>
                                    <a className="geo-card-link" dangerouslySetInnerHTML={{__html: this.props.locale === 'ru' ? "Подробнее    &xrarr;":"Learn more    &xrarr;"}}/>
                                </div>
                            ))}

                        </div>
                        <Button style={{
                            display: this.state.loadMore ? "block" : "none",
                        }}
                                className="geo-show-more"
                                onClick={this.showAllCards}>
                            {this.props.locale === 'ru' ? "Загрузить ещё" : "Load more"}
                        </Button>
                    </div>

                <div className="geo-map-large">

                </div>

                {this.props.locale == 'ru' ?
                    <div className="map-overlay">
                        <span>География<br/> ГК ТАЛТЭК</span>
                        <span>
                        Геологические запасы угля<br/>
                        более <b>500 000 000</b> тонн
                    </span>
                    </div>
                    :
                    <div className="map-overlay">
                        <span>GK TALTEK <br/> Geography</span>
                        <span>
                            Geological reserves of coal<br/>
                        over than <b>500 000 000</b> tons
                    </span>
                    </div>
                }

                {this.state.markers.map( (item,idx) => {
                    //const pos = this.getMarkerPos(idx);style={{...pos}} 
                    const cn = item.className.split(' ');
                    return (
                        <div key={idx} className={"marker-tooltip "+cn[0]}>
                            <Popover
                                    content={this.getContent()[idx]}
                                    placement={cn[1]}>
                                <svg width={16} height={16}>
                                    <circle
                                        cx={8}
                                        cy={8}
                                        r={6}
                                        fill="#004c89"
                                        stroke="#ffffff"
                                        strokeWidth={2}
                                    />
                                </svg>
                            </Popover>
                        </div>
                        )
                })}

            </div>
        )
    }
}