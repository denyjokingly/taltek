import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Col, Button} from 'antd';
import moment from 'moment';
import arrow from '../images/arrows-link.svg';

export default class NewsExpanded extends Component {
    state = {
        article: {},
        news:[]
    };

    componentDidMount(){
        window.scrollTo(0,0);
        fetch('/api/card/'+this.props.match.params.newsId)
            .then( res => res.json())
            .then( article => {
                this.setState({article: article});
            })
            .catch(err => console.log(err));

        fetch('/api/card/')
            .then( res => res.json())
            .then( cards => {
                const news = cards.filter( card => card.is_news );

                news.sort( (a,b) => {
                    return moment(b.news_date) - moment(a.news_date);
                });
                this.setState({news: news});
            })
            .catch(err => console.log(err));
    }

    getPrevItem = () => {
        const {article, news} = this.state;
        const filtered = news.filter( item => item.tags === article.tags);
        let targetId;

        if(article.id === filtered[0].id) {
            targetId = filtered[filtered.length - 1].id;
        } else {
            targetId = filtered[this.findById(article.id) - 1].id;
        }

       this.props.history.push(`/media/${article.tags}/${targetId}`);
    }

    getNextItem = () => {
        const {article, news} = this.state;
        const filtered = news.filter( item => item.tags === article.tags);
        let targetId;

        if(article.id === filtered[filtered.length - 1].id) {
            targetId = filtered[0].id;
        } else {
            targetId = filtered[this.findById(article.id) + 1].id;
        }

       this.props.history.push(`/media/${article.tags}/${targetId}`);
    }

    findById(id){
        let res;
        this.state.news
        .filter( item => item.tags === this.state.article.tags)
        .map((item, idx) => {
            if(item.id === id) {
                res = idx;
            } 
        });

        return res;
    }

    render() {
        const {article} = this.state;
        if(!article.createdAt) return null;
        return (
            <div className="page-container page-spacer page-news">
                <div className="page-content">
                    <Link to={`/media/${article.tags}`} className="link-override"
                          style={{paddingTop: 50, paddingBottom: 50, display: "block"}}
                          dangerouslySetInnerHTML={{ __html:  article.tags === 'news' ? "&xlarr; Назад к списку новостей" : "&xlarr; Назад к списку пресс-релизов" }}>
                        {}
                    </Link>
                    <p className="tg-s news-date">{moment(article.news_date).format('LL')}</p>
                    <h1>{article['title_'+this.props.locale]}</h1>
                    <div dangerouslySetInnerHTML={{__html: article['text_'+this.props.locale]}}></div>
                </div>

                <div className="page-container other-news-container" style={{display: article.tags === 'video' ? "none":"flex"}}>
                    <div className="page-section page-section--newslinks page-section--border"></div>
                    <div className="page-content">
                    <div className="newslinks-wrapper">
                        <a onClick={this.getPrevItem} dangerouslySetInnerHTML={{__html: article.tags === 'news' ? 
                        this.props.locale === 'ru'? "&xlarr; Предыдущая новость" : "&xlarr; Previous article" 
                        : this.props.locale === "ru" ? "&xlarr; Предыдущий пресс-релиз" : "&xlarr; Previous press-release"}}/>

                        <a onClick={this.getNextItem} dangerouslySetInnerHTML={{__html:article.tags === 'news' ? this.props.locale === 'ru'? "Следующая новость &xrarr;" : "Next article &xrarr;" 
                        : this.props.locale === "ru" ? "Следующий пресс-релиз &xrarr;" : "Next press-release &xrarr;"}}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}