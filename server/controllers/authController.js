const User = require('../models').User;
const bcrypt = require('bcrypt');
const saltRounds = 7;

module.exports = {
    create(req, res){
        bcrypt.hash(req.body.password, saltRounds)
        .then( hash => {
            return User
            .create({
                login: req.body.login,
                password: hash,
                role: req.body.role 
            })
            .then( user => res.status(200).send(user))
            .catch( err => res.status(400).send(err));
        })
    },
    list( req, res){
        return User
        .findAll()
        .then( users => res.status(200).send(users));
    },
    retrieve(req, res) {
        return User
        .findById(req.params.userId)
        .then( user => {
            if(!user){
                return res.status(404).send({
                    message: "пользователь не найден"
                })
            }
            return res.status(200).send(user);
        })
        .catch( err => res.status(400).send(err));        
    },
    update(req, res) {
        return USer
        .findById(req.params.userId)
        .then( user => {
            if(!user){
                return res.status(404).send({
                    message: 'user not found'
                });
            }

            return user
                .update( req.body, {
                    fields: Object.keys(req.body)
                })
                .then(()=> res.status(200).send(user))
                .catch( err => res.status(400).send(err));
        })
        .catch( err => res.status(400).send(err));
    },
    destroy(req, res) {
        return User
            .findById(req.params.userId)
            .then( user => {
                if(!user) {
                    return res.status(400).send({message: 'Страница не найдена' });
                }
                return user
                    .destroy()
                    .then( () => res.status(204).send())
                    .catch( err => res.status(400).send(err));
            })
            .catch( err => res.status(400).send(err));
    },
    authenticate(req, res){
        return User
        .findAll({
            where: {
                login: req.body.login
            }
        })
        .then( user => {
            console.log(user);
            if(!user[0]){
                return res.status(404).send({
                    message: "Пользователь не найден"
                })
            }
            bcrypt.compare(req.body.password, user[0].password)
            .then( success => {
                if(success) return res.status(200).send(user)
                return res.status(403).send({
                    message: "Неправильный пароль"
                })
            })
        })
    }
}