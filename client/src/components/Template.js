import React, {Component} from 'react';
import {Tabs, Row, Col, Button} from 'antd';
import {Controlled as CodeMirror} from 'react-codemirror2';
import "./Template.css";
import 'codemirror/mode/xml/xml';

const TabPane = Tabs.TabPane;

export default class Template extends Component{

    constructor(props) {
        super(props);
        this.state = {
            content: [],
            visible: false,
            value_ru:'',
            value_en:'',
            script: '',
            page: {}
        }
    }

    componentDidMount(){
        this.previewRef = React.createRef();
        const { pageId } = this.props.page;
        fetch(`/api/page/${pageId}`)
            .then( res => res.json())
            .then( page => {
                this.setState({page: page, value_ru: page.text_ru, value_en: page.text_en, script: page.script});
            })
    }

    hide = () => {
        this.setState({
            visible: false,
        });
    };

    handleVisibleChange = (visible) => {
        this.setState({ visible });
    };

    savePage() {
        const {page} = this.state;
        page.text_ru = this.state.value_ru;
        page.text_en = this.state.value_en;
        page.script = this.state.script;

        fetch(`/api/page/${this.state.page.id}`,{
            cache: 'no-cache',
            method:"POST",
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify(this.state.page)
        })
            .then( res => res.json())
            .then( page => this.setState( () => ({page: page}) ))
    }

    render(){
       if(!this.state.page) return null;
       const {value_ru, value_en, script} = this.state;
       return (
           <div className="page-container">
               <Row type="flex" justify="space-between" align="middle">
                   <Col >
                       <h1>{this.state.page.title_ru}</h1>
                   </Col>
                   <Col >
                       <Button type="primary" size="large" onClick={this.savePage.bind(this)}>Сохранить</Button>
                   </Col>
               </Row>
               <Row>
                   <Col>
                       <div ref={this.previewRef} className="page-editor__page-preview-wrapper">
                           <Tabs defaultActiveKey="1">
                               <TabPane tab="Русская версия" key="1">
                                   <CodeMirror
                                       value={value_ru}
                                       className="codemirror-editor"
                                       options={{
                                           mode: 'xml',
                                           lineNumbers: true,
                                           lineWrapping: true,
                                       }}
                                       onBeforeChange={(editor, data, value) => {
                                           this.setState({value_ru:value});
                                       }}
                                       onChange={(editor, data, value) => {
                                       }}
                                   />
                               </TabPane>
                               <TabPane tab="Английская версия" key="2">
                               <CodeMirror
                                   value={value_en}
                                   className="codemirror-editor"
                                   options={{
                                       mode: 'xml',
                                       lineNumbers: true,
                                       lineWrapping: true,
                                   }}
                                   onBeforeChange={(editor, data, value) => {
                                       this.setState({value_en: value});
                                   }}
                                   onChange={(editor, data, value) => {
                                   }}
                               />
                           </TabPane>
                           <TabPane tab="Скрипт" key="3">
                               <CodeMirror
                                   value={script}
                                   className="codemirror-editor"
                                   options={{
                                       mode: 'xml',
                                       lineNumbers: true,
                                       lineWrapping: true,
                                   }}
                                   onBeforeChange={(editor, data, value) => {
                                       this.setState({script: value});
                                   }}
                                   onChange={(editor, data, value) => {
                                   }}
                               />
                           </TabPane>
                           </Tabs>

                       </div>
                   </Col>
               </Row>
           </div>
       )
    }
}