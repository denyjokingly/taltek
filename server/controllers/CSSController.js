const fs = require('fs');
const SRC_PATH = './client/build/static/my.css';

module.exports = {
    retrieve(req, res) {
        const readStream = fs.createReadStream(SRC_PATH);
        readStream.pipe(res);
    },

    upload(req, res) {
        const backup = new fs.copyFileSync(SRC_PATH, './client/build/static/my.css.BACKUP');
        const newFile = fs.writeFileSync(SRC_PATH, req.body.cssFile);
    }
};