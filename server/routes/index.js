const pageCtrl = require("../controllers").pageCtrl;
const sliderCtrl = require("../controllers").sliderCtrl;
const cardCtrl = require("../controllers").cardsCtrl;
const cssCtrl = require("../controllers").cssController;
const socialCtrl = require("../controllers").socialController;
const fileUploadCtrl = require("../controllers").fileUploadController;
const multer = require("multer");
const markerCtrl = require("../controllers").markerController;
const authCtrl = require("../controllers").authController;

module.exports = app => {
  app.get("/api/page", pageCtrl.list);
  app.get("/api/foo", pageCtrl.list);
  app.post("/api/page", pageCtrl.create);
  app.get("/api/pagebn", pageCtrl.retrieveByName);
  app.get("/api/page/:pageId", pageCtrl.retrieve);
  app.post("/api/page/:pageId", pageCtrl.update);
  app.delete("/api/page/:pageId", pageCtrl.destroy);
  app.get("/api/backup", pageCtrl.backup);
  app.post("/api/page/navigation/:pageId", pageCtrl.toggleMain);
  app.post("/api/page/visibility/:pageId", pageCtrl.toggleVisible);

  app.get("/api/slider", sliderCtrl.list);
  app.get("/api/slider/:sliderId", sliderCtrl.retrieve);
  app.post("/api/slider", sliderCtrl.create);
  app.post("/api/slider/:sliderId", sliderCtrl.update);
  app.delete("/api/slider/:sliderId", sliderCtrl.destroy);

  app.get("/api/card", cardCtrl.list);
  app.get("/api/card/:cardId", cardCtrl.retrieve);
  app.post("/api/card", cardCtrl.create);
  app.post("/api/card/:cardId", cardCtrl.update);
  app.delete("/api/card/:cardId", cardCtrl.destroy);

  app.get("/api/css", cssCtrl.retrieve);
  app.post("/api/css", cssCtrl.upload);

  app.get("/api/social", socialCtrl.list);
  app.get("/api/social/:socialId", socialCtrl.retrieve);
  app.post("/api/social", socialCtrl.create);
  app.post("/api/social/:socialId", socialCtrl.update);
  app.delete("/api/social/:socialId", socialCtrl.destroy);

  app.post("/api/upload/doc", fileUploadCtrl.uploadDoc);
  app.get("/api/upload/doc", fileUploadCtrl.retrieveDocs);
  app.delete("/api/upload/doc/:file", fileUploadCtrl.deleteDoc);
  app.get("/api/upload/doc/:file", fileUploadCtrl.downloadFile);
  app.post("/api/upload/remote/asset", fileUploadCtrl.downloadAsset);
  app.post("/api/upload/asset", fileUploadCtrl.uploadAsset);
  app.get("/api/upload/asset", fileUploadCtrl.retrieveAssets);
  app.delete("/api/upload/asset/:file", fileUploadCtrl.deleteAsset);

  app.get("/api/marker", markerCtrl.list);
  app.post("/api/marker", markerCtrl.create);
  app.get("/api/marker/:markerId", markerCtrl.retrieve);
  app.post("/api/marker/:markerId", markerCtrl.update);
  app.delete("/api/marker/:markerId", markerCtrl.destroy);

  app.post("/api/user", authCtrl.create);
  app.post("/api/user/authenticate", authCtrl.authenticate);
};
