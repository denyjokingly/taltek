import React, {Component} from 'react';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import logoBlack from '../images/logotype-black.svg';

const FormItem = Form.Item;

class NormalLoginForm extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        this.props.onChange(values);
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const {locale} = this.props;
    
    return (
      <Form onSubmit={this.handleSubmit} className="taltek-login-form">
        <FormItem>
            <img src={logoBlack}/>
        </FormItem>
        <FormItem>
          {getFieldDecorator('userName', {
            rules: [{ required: true, message: 'Пожалуйста введите логин!' }],
          })(
            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Пользователь" />
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Пожалуйста введите пароль!' }],
          })(
            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Пароль" />
          )}
        </FormItem>
        <FormItem>
          {/* {getFieldDecorator('remember', {
            valuePropName: 'checked',
            initialValue: true,
          })(
            <Checkbox>Запомнить меня</Checkbox>
          )} */}
          <Button type="primary" htmlType="submit" className="login-form-button">
            Войти
          </Button>
        </FormItem>
      </Form>
    );
  }
}

const Login = Form.create()(NormalLoginForm);

export default Login;