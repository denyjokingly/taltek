$(document).ready(function() {
  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == " ") {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  function buildModalFor(item) {
    var newsModal = document.createElement("div");
    newsModal.classList.add("newsModal");
    newsModal.innerHTML = `
		<div id="iconMenuBarHeader">
	    <div class="lineMenuBarHeader"></div>
	    <div class="lineMenuBarHeader"></div>
	  </div>

		<div class="newsBlock">

		  <div class="news-item-date">${moment(item.createdAt).format("LL")}</div>
			<h1 class="news-item-header">${item["title_" + lang]}</h1>

			<div id="contentNews">
          ${item["text_" + lang]}
			</div>

  </div>`;
    document.body.appendChild(newsModal);
    var close = document.querySelector("div.newsModal #iconMenuBarHeader");
    close.addEventListener("click", function(e) {
      e.preventDefault();
      document.body.removeChild(newsModal);
    });

    var posInnerPage = $("#internalPages").offset();
    posScrollPage = $(window).scrollTop();

    $(".newsModal").css({
      left: posInnerPage.left,
      "-webkit-transform":
        "translateX(" + $("#internalPages").innerWidth() + "px)"
    });

    if ($(window).width() < 1200) {
      $(".newsModal").css({
        width: $(window).innerWidth() - 40,
        height: $(window).innerHeight() - 15
      });
    }
  }

  function NewsFeed(data) {
    if (!this instanceof NewsFeed) {
      return new NewsFeed(data);
    }

    this.page = 0;
    this.prevPage = 0;
    this.pages_max = Math.ceil(data.length / 10);
    this.data = data;
    this.pages_DOM = [];

    this.container = document.createElement("div");
    this.container.classList.add("pageGroup");
    this.container.id = "news";

    this.container_inner = document.createElement("div");
    this.container_inner.classList.add("titleGroup", "pageGroupPattern");

    this.header = document.createElement("div");
    this.header.classList.add("headPageUnit");
    this.header.innerHTML = `<h1>${lang === "ru" ? "новости" : "news"}</h1>`;

    this.news_container = document.createElement("div");
    this.news_container.classList.add("newsGroup");

    this.pager = document.createElement("div");
    this.pager.classList.add("pager");
    this.pagerElPrevCtrl = document.createElement("div");
    this.pagerElPrevCtrl.classList.add(
      "controlPager",
      "navControl",
      "prevNavControl"
    );
    this.pagerElNextCtrl = document.createElement("div");
    this.pagerElNextCtrl.classList.add(
      "controlPager",
      "navControl",
      "nextNavControl"
    );

    this.pager.appendChild(this.pagerElPrevCtrl);
    this.pager.appendChild(this.pagerElNextCtrl);

    this.container_inner.appendChild(this.header);
    this.container_inner.appendChild(this.news_container);
    this.container_inner.appendChild(this.pager);
    this.container.appendChild(this.container_inner);

    this.pagerElNextCtrl.addEventListener(
      "click",
      function(e) {
        e.preventDefault();
        this.step(1);
      }.bind(this)
    );

    this.pagerElPrevCtrl.addEventListener(
      "click",
      function(e) {
        e.preventDefault();
        this.step(-1);
      }.bind(this)
    );

    this.init = function() {
      this.data.forEach(
        function(item) {
          var node = document.createElement("div");
          node.classList.add("news-item-container");
          var link = document.createElement("a");
          link.classList.add("news-item-header", "newsLoad");
          link.innerHTML = item["title_" + lang];

          link.addEventListener(
            "click",
            function(e) {
              e.preventDefault();

              buildModalFor(item);
              var posInnerPage = $("#internalPages").offset();

              $("body").addClass("newsPage");
              console.log($("body"));

              $(".newsModal").css({
                left: posInnerPage.left,
                "-webkit-transform": "translateX(0px)"
              });

              if ($(window).width() < 1200) {
                $(".newsModal").css({
                  width: $(window).innerWidth() - 40,
                  height: $(window).innerHeight() - 15
                });
              }

              $("body").on("touchmove", function(event) {
                event.preventDefault();
              });
            }.bind(this)
          );

          var ts = document.createElement("span");
          ts.classList.add("news-item-date");
          ts.innerHTML = moment(item.createdAt).format("LL");

          node.appendChild(link);
          node.appendChild(ts);

          this.pages_DOM.push(node);
        }.bind(this)
      );

      for (var i = 0; i < this.pages_max; i++) {
        var anchor = document.createElement("a");
        anchor.classList.add("numbPager");
        if (i === this.page) anchor.classList.add("active");
        anchor.setAttribute("data-pageIdx", i);
        anchor.textContent = i + 1;

        anchor.addEventListener(
          "click",
          function(e) {
            e.preventDefault();
            this.gotoPage(e.target.getAttribute("data-pageIdx"));
          }.bind(this)
        );

        this.pager.insertBefore(
          anchor,
          this.pager.children[this.pager.children.length - 1]
        );
      }

      this.renderPage();
      this.renderPager(1, 1);
    }.bind(this);

    this.step = function(direction) {
      this.prevPage = this.page;
      this.page += parseInt(direction);

      if (this.page < 0) {
        this.page = this.pages_max - 1;
      } else if (this.page > this.pages_max - 1) {
        this.page = 0;
      }

      this.renderPage();
      this.renderPager();
    }.bind(this);

    this.gotoPage = function(page) {
      this.prevPage = this.page;
      this.page = parseInt(page);

      this.renderPage();
      this.renderPager();
    }.bind(this);

    this.renderPage = function() {
      var start_idx = this.page * 10 + 1;
      var end_idx = start_idx + 10;
      var page = this.pages_DOM.slice(start_idx, end_idx);

      this.news_container.children.length &&
        Array.from(this.news_container.children).forEach(
          function(item) {
            this.news_container.removeChild(item);
          }.bind(this)
        );

      page.forEach(
        function(item) {
          this.news_container.appendChild(item);
        }.bind(this)
      );
    }.bind(this);

    this.renderPager = function() {
      var findByIdx = function(idx) {
        return document.querySelector(`a[data-pageidx='${idx}']`);
      };

      findByIdx(this.prevPage) &&
        findByIdx(this.prevPage).classList.remove("active");
      findByIdx(this.page) && findByIdx(this.page).classList.add("active");
    }.bind(this);

    this.init();

    return {
      gotoPage: this.gotoPage,
      step: this.step,
      renderPage: this.renderPage,
      renderPager: this.renderPager,
      init: this.init,
      root: this.container
    };
  }

  var lang = getCookie("prefLang") || "ru";
  moment.locale(lang);

  //Setup Photo Cards

  function setupPhotoInnerPage() {
    $(".cardPhotoUnit").each(function(i, elem) {
      $(this).css({
        "background-image": "url(" + $(this).attr("data-cards-back") + ")"
      });
    });

    $(".mapImage").each(function(i, elem) {
      $(this).css({
        "background-image": "url(" + $(this).attr("data-map-back") + ")"
      });
    });

    if ($(window).width() > 767) {
      $(".cardsAboutPattern.longRightPhoto .cardPhotoUnit").each(function(
        i,
        elem
      ) {
        var posEl = $(this).prev();
        posCardUnit = posEl.offset();
        posPages = $("#internalPages").offset();

        $(this).css({
          left: posCardUnit.left + posEl.innerWidth() + 30 - posPages.left,
          width:
            $(window).innerWidth() - posCardUnit.left - posEl.innerWidth() - 90
        });
      });

      $(".rightPhotoAboutPattern .photoRightPhotoAboutPattern").each(function(
        i,
        elem
      ) {
        var posEl = $(this).prev();
        posTextRight = posEl.offset();
        posPages = $("#internalPages").offset();

        $(this).css({
          left: posTextRight.left + posEl.innerWidth() - posPages.left + 1,
          width:
            $(window).innerWidth() -
            posTextRight.left -
            posEl.innerWidth() -
            20,
          height: posEl.innerHeight() + 10
        });

        if ($(this).hasClass("fullBlock") == true) {
          $(this).css({
            height: posEl.innerHeight() + 242
          });
        }
      });
    }

    if ($(window).width() < 1361) {
      $(".pageGroup")
        .addClass("w768")
        .removeClass("w960");
    }

    if ($(window).width() < 1200) {
      $(".pageGroup")
        .removeClass("w768")
        .addClass("w960");
    }

    if ($(window).width() < 1024) {
      $(".pageGroup")
        .addClass("w768")
        .removeClass("w960");
    }

    if ($(window).width() < 768) {
      $(".pageGroup")
        .addClass("wMobile")
        .removeClass("w960")
        .removeClass("w768");
      $(".aboutMap").css({
        "margin-bottom": $(".cardNoteUnit.show").innerHeight() - 30
      });
    }
  }

  function setupMapInnerPage() {
    $(".picAboutMap").hover(function() {
      var dataPic = $(this).attr("data-pic-map");
      showPic = "#" + dataPic + "CardNoteUnit";

      $(".cardNoteUnit")
        .removeClass("show")
        .addClass("hide");
      $(showPic)
        .removeClass("hide")
        .addClass("show");
    });

    //Setup Data control for Hystory Slides
    $(".slideHistory").each(function(i, elem) {
      $(this).attr("data-control-slide", i);
      $(this).attr("id", "slideHistory-" + i);
    });

    $(".sliderHistory").css({ height: $(".slideHistory.show").innerHeight() });

    //Action Control Link Slide
    $(".sliderHistory .prevNavControl").click(function() {
      currentSlide = $(".slideGroup .show").attr("data-control-slide");
      nextSlide = parseInt(currentSlide) - 1;
      lastSlide = $(".slideGroup div:last-child").attr("data-control-slide");

      if (nextSlide < 0) {
        var nextSlide = lastSlide;
      }

      $("#slideHistory-" + currentSlide)
        .addClass("hide")
        .removeClass("show");
      $("#slideHistory-" + nextSlide)
        .removeClass("hide")
        .addClass("show");

      $(".sliderHistory").css({
        height: $(".slideHistory.show").innerHeight()
      });
    });

    $(".sliderHistory .nextNavControl").click(function() {
      currentSlide = $(".slideGroup .show").attr("data-control-slide");
      nextSlide = parseInt(currentSlide) + 1;
      lastSlide = $(".slideGroup div:last-child").attr("data-control-slide");

      if (nextSlide > lastSlide) {
        var nextSlide = 0;
      }

      $("#slideHistory-" + currentSlide)
        .addClass("hide")
        .removeClass("show");
      $("#slideHistory-" + nextSlide)
        .removeClass("hide")
        .addClass("show");

      $(".sliderHistory").css({
        height: $(".slideHistory.show").innerHeight()
      });
    });
  }

  function setupNewsPage() {
    var posInnerPage = $("#internalPages").offset();
    posScrollPage = $(window).scrollTop();

    $(".newsModal").css({
      left: posInnerPage.left,
      "-webkit-transform":
        "translateX(" + $("#internalPages").innerWidth() + "px)"
    });

    if ($(window).width() < 1200) {
      $(".newsModal").css({
        width: $(window).innerWidth() - 40,
        height: $(window).innerHeight() - 15
      });
    }
  }

  $("a.newsLoad").on("click", function(e) {
    var posInnerPage = $("#internalPages").offset();

    $("body").addClass("newsPage");
    console.log($("body"));

    $(".newsModal").css({
      left: posInnerPage.left,
      "-webkit-transform": "translateX(0px)"
    });

    if ($(window).width() < 1200) {
      $(".newsModal").css({
        width: $(window).innerWidth() - 40,
        height: $(window).innerHeight() - 15
      });
    }

    $("body").on("touchmove", function(event) {
      event.preventDefault();
    });

    e.preventDefault();
  });

  $(".newsModal #iconMenuBarHeader").on("click", function(e) {
    $("body")
      .removeClass("newsPage")
      .css("position", "initial");
    $(".newsModal").css({
      "-webkit-transform":
        "translateX(" + $("#internalPages").innerWidth() + "px)"
    });
  });

  setupPhotoInnerPage();
  setupMapInnerPage();
  setupNewsPage();

  $("a.jumpToPage").on("click", function(e) {
    var page_url = this.href.split("http://")[1];
    var host = location.host;
    page_url = page_url.replace(host, "");

    if (page_url === "/news") {
      $.ajax({
        type: "GET",
        url: "/api/card/",
        success: function(data) {
          var nf = new NewsFeed(
            data.filter(function(item) {
              return item.is_news && item.tags === "news";
            })
          );

          $("#contentPage").html("");
          $("#contentPage").append(nf.root);

          window.history.pushState(
            $("#contentPage").html(),
            lang === "ru" ? "новости" : "news",
            page_url
          );
        }
      });
    } else {
      $.ajax({
        type: "GET",
        url: `/api/pagebn?name=${page_url}`,
        success: function(data) {
          var lang = getCookie("prefLang") || "ru";
          var stripped = data["text_" + lang].replace(
            /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
            ""
          );
          var footer = document.querySelector("#footer").outerHTML;
          $("#contentPage").html(stripped + footer);
          window.history.pushState(stripped, data.title_ru, page_url);
        }
      });
    }
    var urlName = $(this).attr("href");
    urlBack = "url(/static/media/jpg" + urlName + "-back.jpg)";
    urlContent = "<img src='/static/media/jpg" + urlName + ".jpg' />";
    urlLayout = "/content" + urlName;

    $("body")
      .removeClass("front")
      .addClass("innerPage");
    $(".contentSlideBigSlider")
      .removeClass("show")
      .addClass("hide");
    $("#controlSlideBigSlider")
      .removeClass("show")
      .addClass("hide");

    if ($(window).width() > 1199) {
      $("#menu")
        .removeClass("close")
        .addClass("open");
      $("#mainMenu").show();

      $(".photoSlideBigSlider").css("width", "40%");
    }

    if ($(window).width() < 1200) {
      $(".innerPage header")
        .addClass("light")
        .removeClass("dark");
    }

    if (
      $("#header").hasClass("openMenu") == true &&
      $("#header").hasClass("openMap") == false
    ) {
      $("#header")
        .removeClass("openMenu")
        .addClass("closeMenu");
      $("#footer")
        .removeClass("openMenu")
        .addClass("closeMenu");

      $(".linkMenuBarHeader:nth-child(1)").show();
      $(".linkMenuBarHeader:nth-child(2)").hide();
      $(".linkMenuBarHeader:nth-child(3)").hide();

      $("#menuModal")
        .removeClass("open")
        .addClass("close");

      $(".backModal")
        .removeClass("show")
        .addClass("hide");

      $("ymaps").remove();

      if ($(window).width() > 1199) {
        $("#menuBarHeader")
          .delay(1000)
          .hide(1000);
      }

      if ($(window).width() < 1200) {
        $("#menu")
          .removeClass("open")
          .addClass("close");
        $("#mainMenu")
          .delay(1000)
          .hide(1000);
      }
    }

    // $("#contentPage").load(urlLayout);
    $("#backPage").css({ "background-image": urlBack });

    $("#backPage").css({ "z-index": 1 });

    $("#logoTaltekHeader svg").addClass("closeToPage");
    $("#buttonBackHome").addClass("closeToPage");

    setTimeout(function() {
      $("#backPage").css({ opacity: 1 });
      setupPhotoInnerPage();
      setupMapInnerPage();
    }, 1500);

    e.preventDefault();
  });

  $(".closeToPage").on("click", function(e) {
    if ($("body").hasClass("innerPage") == true) {
      $("#backPage").css("opacity", "0");

      setTimeout(function() {
        $("body")
          .removeClass("innerPage")
          .addClass("front");

        $("#menu")
          .removeClass("open")
          .addClass("close");
        $("#menuBarHeader").show();
        $("#mainMenu")
          .delay(1000)
          .hide(1000);

        $(".photoSlideBigSlider").css("width", "100%");

        $(".contentSlideBigSlider")
          .removeClass("hide")
          .addClass("show");
        $("#controlSlideBigSlider")
          .removeClass("hide")
          .addClass("show");

        $("#backPage").css({ "z-index": -1 });
        window.history.pushState("", "", "/");
      }, 500);
    }

    e.preventDefault();
  });
});
