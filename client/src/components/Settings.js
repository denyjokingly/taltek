import React, {Component} from 'react';
import {Divider, List, Button, Icon, Popconfirm} from 'antd';
import {Link} from 'react-router-dom';

export default class Settings extends Component {
    state = {
      socials:[]
    };

    getSocials() {
        fetch('/api/social')
            .then( res => res.json())
            .then( socials => this.setState({socials: socials}))
            .catch( err => console.log(err))
    }

    componentDidMount() {
        this.getSocials();
    }

    deleteSocial(itemId){
        fetch(`/api/social/${itemId}`,{
            method: "DELETE"
        })
            .then( () => this.getSocials())
            .catch( err => console.log(err) );
    }

    render() {
        return (
            <div className="page-container">
                <h1>Список соц. сетей :</h1>
                <Divider />
                <List
                    itemLayout="horizontal"
                    dataSource={this.state.socials}
                    size="small"
                    style={{ width: 600, margin: "40px auto"}}
                    renderItem= { item => (
                        <List.Item actions={[<Link style={{ textDecoration: "none", color: "#a1a1a1"}}  to={`/admin/edit/social/${item.id}`}><Icon type="edit"/></Link>,
                            <Popconfirm title="Подтвердите действие" 
                            okText="Да" 
                            cancelText="Нет" 
                            onCancel={ () => console.log('cancel')} 
                            onConfirm={ this.deleteSocial.bind(this,`${item.id}`) }><a style={{ textDecoration: "none", color: "#a1a1a1"}}><Icon type="close"/></a></Popconfirm> ]}>
                            {item.title}
                        </List.Item>
                    )}
                />
                <Link to="/admin/create/social" className="pages-list__add-button">
                    <Button type="dashed" style={{ width: '60%' }}>
                        <Icon type="plus" /> Добавить соц. сеть
                    </Button>
                </Link>
            </div>
        )
    }
}