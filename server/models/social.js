'use strict';
module.exports = (sequelize, DataTypes) => {
  var Social = sequelize.define('Social', {
    title: DataTypes.STRING,
    link: DataTypes.STRING,
    img: DataTypes.STRING
  }, {});
  Social.associate = function(models) {
    // associations can be defined here
  };
  return Social;
};