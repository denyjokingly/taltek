$(document).ready(function() {

    var wScreen = $(window).width();
        hScreen = $(window).height();

    if (wScreen > 1023) {

      $("#langHeader").css({ "left": 0.5*$(window).innerWidth() });
      $("#socialFooter").css({ "left": 0.5*$(window).innerWidth() });

      //Big Slider
      //Load Photo
      $('.photoSlideBigSlider').each(function(i,elem) {

        var urlBack = $(this).attr('data-photo-slider');

            $(this).css('background-image', 'url(' + urlBack + ')');

      });

    }


    if (wScreen < 1200) {

      $("#mainMenu").css({ "top": 0.5*$(window).innerHeight() - 0.5*$("#mainMenu").innerHeight() });
      $("#mainMenu.forInternalPages").css({ "top": 0.5*$(window).innerHeight() - 0.5*$("#mainMenu").innerHeight() });
      $("#contactsMenuModal").css({ "top": 0.5*$(window).innerHeight() - 0.5*$("#mainMenu").innerHeight() - 70 });
      $(".innerPage header").addClass('light').removeClass('dark');

      $("#menu").addClass("close").removeClass("open");
			$("#mainMenu").hide();

      $("#textMenuBarHeader").remove();
      $("#backPage").remove();

    }

    if (wScreen < 1024) {

      var wLink = $(window).innerWidth() / $('.linkControlSlideBigSlider').length - 4;

      $("#maps").remove();
			$("#contentContactsMenuModal").remove();
			$("#tabContacts").remove();
      $("#textMenuBarHeader").remove();
      $(".linkControlSlideBigSlider").html('').css({ "width": wLink });

    }

    if (wScreen < 768) {

      $("#maps").remove();
      $("#contentContactsMenuModal").remove();
      $("#tabContacts").remove();
      $("#textMenuBarHeader").remove();

      $(".backModal .filter").css({
        "height": $(window).innerHeight() - 10,
        "width": $(window).innerWidth() - 10
      });

      $("#mainMenu").css({ "top": 80 });

    }


    if (hScreen < 600) {

      $("#maps").remove();
			$("#contentContactsMenuModal").remove();
			$("#tabContacts").remove();
      $("#textMenuBarHeader").remove();

    }


    //Big Slider
    //Load Photo
    //Load Video
    if (wScreen > 1023) {

      $('.photoSlideBigSlider').each(function(i,elem) {
        var urlBack = $(this).attr('data-photo-slider');
            $(this).css('background-image', 'url(' + urlBack + '.jpg)');
      });

      $('.slideBigSlider:first-child .photoSlideBigSlider').html("<video autoplay muted loop id='myVideo'><source src='/media/video/taltek2.mp4' type='video/mp4'></video>'");

    }

    if ( wScreen < 1024 && wScreen > 767 ) {

      $('.photoSlideBigSlider').each(function(i,elem) {
        var urlBack = $(this).attr('data-photo-slider');
            $(this).css('background-image', 'url(' + urlBack + '-tablet.jpg)');
      });

      $('.slideBigSlider:first-child .photoSlideBigSlider').html("<video autoplay muted loop id='myVideo'><source src='/media/video/taltek-tablet.mp4' type='video/mp4'></video>'");

    }

    if (wScreen < 768) {

      $('.photoSlideBigSlider').each(function(i,elem) {
        var urlBack = $(this).attr('data-photo-slider');
            $(this).css('background-image', 'url(' + urlBack + '-mobile.jpg)');
      });

      $('.slideBigSlider:first-child .photoSlideBigSlider').html("<video autoplay muted loop id='myVideo'><source src='/media/video/taltek-mobile.mp4' type='video/mp4'></video>'");

    }



	});
