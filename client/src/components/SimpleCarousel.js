import React, { Component } from 'react';
import {Carousel} from 'antd';
import './SimpleCarousel.css';


function SamplePrevArrow(props) {
    const { klass, style, onClick } = props;
    return (
      <div
        className={klass}
        style={{ ...style, display: "block",
        backgroundImage: "url(/static/upload/assets/arrows.svg)",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        content: "none",
        height: 40,
        width: 40,
        position:"absolute",
        cursor: 'pointer',
        zIndex: 99}}
        onClick={onClick}
      />
    );
  }

const options = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide:1,
    arrows: true,
    prevArrow: <SamplePrevArrow klass='prevArrow'/>,
    nextArrow: <SamplePrevArrow klass='nextArrow'/>,
    appendDots: dots => (
        <div>
            <ul className="simple-carousel-wrapper__controls"> {dots} </ul>
        </div>
    ),
    customPaging: i => (
        <div className="simple-carousel-wrapper__controls__dot">
        </div>
    )
};

export default class SimpleCarousel extends Component{

    state = {
        slider: {}
    };

    componentDidMount() {
        this.setState({ref: React.createRef()});
        fetch('/api/slider')
            .then(res => res.json())
            .then( sliders => {
                const [slider] = sliders.filter( slider => slider.content.id == 'bottom-slider');
                this.setState({slider:slider});
            });
            let int = setInterval( ()=> {
                if(this.state.ref && window.location.path == "/"){
                    this.state.ref.current.slick.slickGoTo(0);
                    clearInterval(int);
                }
            },1000);
    }

    setSlide = e => {
        // console.log(this.state.ref);
    }

    render(){
        return (
            <div className="simple-carousel-wrapper">
                <Carousel ref={this.state.ref} {...options} onInit={this.setSlide}>
                    {this.state.slider.content && this.state.slider.content.slides.map( (x,i) =>{
                        return (<div className="simple-carousel-wrapper__inner" key={i}>
                            <div className="simple-carousel-wrapper__image" src={x.img} style={{backgroundImage: "url("+x.img.split('').map(
                                    c => c === '\\'? "/": c).join('')+")", backgroundPosition: "cover"}}/>
                            <div className="simple-carousel-wrapper__description" dangerouslySetInnerHTML={{__html: x['text_'+this.props.locale]}}>
                            </div>
                        </div>)
                    })}
                </Carousel>

            </div>
        )
    }
}