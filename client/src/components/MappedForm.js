import React, {Component} from 'react';
import { Form, Input, Button } from 'antd';

const FormItem = Form.Item;

export default class MappedForm extends Component{

    state = {
        slider: this.props.slider
    };

    saveSlider(){
        const {slider} = this.state;

        debugger;

        fetch(`/api/slider/${slider.id}`,{
            method: 'POST',
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify(slider)
        })
            .then( res => res.json())
            .then( slider => this.setState({slider: slider}) )
    }


    handleSubmit = e => {
      e.preventDefault();

      this.props.form.validateFields((err, values)=>{
          const {slider} = this.state;

          Object.keys(values).map( (key) => {
              slider.content.slides.map( slide => {
                  slide[key] = values[key]
                  return void 0;
              })
              return void 0;
          });
          this.setState({slider: slider});
      })
    };

    addSlide(){
        const updating = this.state.slider;
        updating.content.slides.push({url:'',text:''});
        this.setState({slides: updating});
    }

    render(){
        const {getFieldDecorator} = this.props.form;
        const {slider} = this.state;
        const {slides} = slider.content;
        if(!slider) return null;
        return (
            <Form onChange={this.handleSubmit}>
                { slides.map( (slide, idx) => {
                    return(
                        <div key={idx}>
                            <h3>Слайд № {idx+1}</h3>
                            <FormItem label="URL картинки">
                                { getFieldDecorator('img'+(idx+1),{initialValue: slide['img'+(idx+1)]})(<Input type="text"/>)}
                            </FormItem>
                            <FormItem label="URL страницы">
                                { getFieldDecorator('url'+(idx+1),{initialValue: slide['url'+(idx+1)]})(<Input type="text"/>)}
                            </FormItem>
                            <FormItem label="Подпись">
                                { getFieldDecorator('text'+(idx+1),{initialValue: slide['text'+(idx+1)]})(<Input type="text" />)}
                            </FormItem>
                        </div>)
                })}
                <FormItem>
                    <Button style={{marginRight: 10}} onClick={this.addSlide.bind(this)} type="primary">Добавить слайд</Button>
                    <Button type="primary"onClick={this.saveSlider.bind(this)}>Сохранить</Button>
                </FormItem>
            </Form>
        )
    }
}