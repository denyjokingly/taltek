import React, {Component} from 'react';
import { Form, Input, Button, message, Row, Col } from 'antd';

const FormItem = Form.Item;
const success = () => {
    message.success('Изменения успешно применены');
};
const error = () => {
    message.error('Произошла ошибка');
};
const CustomizedForm = Form.create({
    onFieldsChange(props, changedFields){
        props.onChange(changedFields);
    },
    mapPropsToFields(props) {
        return {
            img: Form.createFormField({
                ...props.img,
                value: props.img.value,
            }),
            link: Form.createFormField({
                ...props.link,
                value: props.link.value
            }),
            title_ru: Form.createFormField({
                ...props.title_ru,
                value: props.title_ru.value
            }),
            title_en: Form.createFormField({
                ...props.title_en,
                value: props.title_en.value
            }),
            is_news: Form.createFormField({
                ...props.is_news,
                value: props.is_news.value
            }),
            tags: Form.createFormField({
                ...props.tags,
                value: props.tags.value
            }),
            text: Form.createFormField({
                ...props.text,
                value: props.text.value
            })
        };
    },
    onValuesChange(_, values) {
        //console.log(values);
    },
})((props) => {
    const { getFieldDecorator } = props.form;
    return (
        <Row>
            <Col span={12} offset={1}>
                <Form onSubmit={props.onSubmit}>
                    <FormItem
                        label="Ссылка на картинку)">
                        {getFieldDecorator('img',{
                            rules: [{required: false, message: "Поле не должно быть пустым"}]
                        })(<Input />)}
                    </FormItem>
                    <FormItem
                        label="Ссылка на страницу">
                        {getFieldDecorator('link',{
                            rules: [{required: false, message: "Поле не должно быть пустым"}]
                        })(<Input disabled={props.is_news.value} />)}
                    </FormItem>
                    <FormItem
                        label="Заголовок (русский)">
                        {getFieldDecorator('title_ru',{
                            rules: [{required: false, message: "Поле не должно быть пустым"}],
                        })(<Input />)}
                    </FormItem>
                    <FormItem
                        label="Заголовок (английский)">
                        {getFieldDecorator('title_en',{
                            rules: [{required: false, message: "Поле не должно быть пустым"}],
                        })(<Input />)}
                    </FormItem>
                    <Button type="primary"
                            htmlType="submit"
                            disabled={
                                props.img === '' ||
                                props.link === '' ||
                                props.title_ru === ''
                            }
                    >Сохранить</Button>
                    {!props.isNew && <Button type="danger" onClick={props.deletePage} style={{marginLeft: "5px"}}>Удалить</Button>}
                </Form>
            </Col>
        </Row>
    );
});

message.config({
    maxCount: 1,
    duration:2,
});

export default class Page extends Component {
    constructor() {
        super();
        this.state = {
            isNew: true,
            fields: {
                img: {
                    value: ''
                },
                link: {
                    value: ''
                },
                title_ru: {
                    value: ''
                },
                title_en: {
                    value: ''
                },
                is_news: {
                    value: false
                },
                tags: {
                    value: ''
                },
                text: {
                    value: ''
                },
            },
            content: []
        };
    }

    componentDidMount() {
        this.getCard();
    }

    getCard() {
        const { match: { params } } = this.props;
        if( !params.pageId ) return;
        fetch(`/api/card/${params.pageId}`)
            .then( res => res.json())
            .then( card => this.patchFields(card));
    }

    deletePage(){
        const { match: { params } } = this.props;
        if( !params.pageId ) return;
        fetch(`/api/page/${params.pageId}`,{
            method: 'DELETE',
        })
            .then( () => this.props.history.push('/dashboard/cards'))
            .catch( err => console.log(err));

    }

    handleFormChange = (changedFields) => {
        this.setState(({ fields }) => ({
            fields: { ...fields, ...changedFields },
        }));
    };

    patchFields(card) {
        const updatedFields = Object.assign({}, this.state.fields);
        for(let prop in card){
            if(updatedFields[prop]) updatedFields[prop].value = card[prop];
        }
        this.setState((state) => ({ fields: updatedFields, isNew: false }));
    }


    sendUpdate = (e) => {
        e.persist();
        e.preventDefault();

        let cardToUpdate = {
            img: this.state.fields.img.value,
            link: this.state.fields.link.value,
            title_ru: this.state.fields.title_ru.value,
            title_en: this.state.fields.title_en.value,
            is_news: this.state.fields.is_news.value,
            tags: this.state.fields.tags.value,
            text: this.state.fields.text.value,
        };

        const { match: { params } } = this.props;
        const route = this.state.isNew ? '' : `${params.pageId}`;

        fetch(`/api/card/${route}`, {
            method: 'POST',
            cache: 'no-cache',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify(cardToUpdate)
        })
            .then( res => res.json() )
            .then( card => {
                success();
                this.patchFields(card)
            })
            .catch( err=> {
                error();
                console.log(err);
                this.getPage();
            })
    };

    render(){
        const fields = this.state.fields;

        return (
            <CustomizedForm {...fields} onChange={this.handleFormChange} deletePage={this.deletePage.bind(this)} isNew={this.state.isNew} onSubmit={this.sendUpdate}/>
        )
    }
}