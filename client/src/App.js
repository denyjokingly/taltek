import React, { Component } from 'react';
import {
  Switch,
  Route, Link
} from 'react-router-dom';
import './App.css';
import './slick-theme.css';
import './slick.css';
import Main from './components/Main';
import Dashboard from './components/Dashboard';
import BlankPage from './components/BlankPage';
import { Layout, Menu, Popover } from 'antd';
import logoMain from './images/logotype-black.svg';
import logoWhite from './images/logotype-white.svg';
import logoKody from './images/kody.png';
import barOpened from './images/bar-open.svg';
import barClosed from './images/bar-close.svg';
import { Select } from 'antd';
import Login from './components/Login';

const Option = Select.Option;
const { Content, Footer } = Layout;

class App extends Component {
  state = {
      nav:[],
      locale: this.getLocale(),
      socials: [],
      mmref: React.createRef(),
      mmstate: false,
    };

  getLocale() {
      const savedLocale = localStorage.getItem('prefLocale');
      const lang = navigator.language.slice(0,2) || 'ru';

      if(!savedLocale){
          localStorage.setItem('prefLocale', lang);
      }

      return savedLocale || lang;
  }

  componentDidMount() {
      fetch('/api/page')
          .then(res=> res.json())
          .then(pages => {
              const nav = [];
              pages.map( page => {
                  if(page.is_main) {
                      nav.push({parent: page, children:[]});
                  }
                  return void 0;
              });
              const bottom = pages.filter( page => !page.is_main);

              nav.map( page => {
                  bottom.map( bottomPage => {
                      if(page.parent.id === bottomPage.childPageId) {
                          page.children.push(bottomPage)
                      }
                      return void 0;

                  });
                  return void 0;
              });
              nav.sort((a, b) => {
                  return a.parent.idx - b.parent.idx;
              });
              nav.map( node => {
                  node.children.sort( (a,b) => {
                      return a.idx - b.idx;
                  });
                  return void 0;
              });
              this.setState({nav: nav});
              return void 0;
          });

      fetch('/api/social')
          .then(res=>res.json())
          .then(socials => {
            
            socials.map( social => {
                const url = social.img;
                if(url.match(/\.svg/)){
                    new Promise( resolve => {
                        const fileXhr = new XMLHttpRequest();
                        fileXhr.open('GET',url, true);
                        fileXhr.addEventListener('load',  function(e) {
                            social.svg = this.responseText;
                            resolve(social);
                        });
                        fileXhr.send();
                    })
                    .then( () => this.setState({socials: socials}))

                }
            }); 
          });

      const locale = this.getLocale();
      document.querySelector('html').setAttribute('lang',locale);
  }


  handleSelect = (locale) => {
      this.setState({locale: locale});
      localStorage.setItem('prefLocale', locale);
      document.querySelector('html').setAttribute('lang',locale);
  };

  toggleMenu = e => {
      const {mmstate} = this.state;
      this.setState({mmstate: !mmstate});
  };

  render() {
    const locale = this.getLocale();
    return (
      <Layout className={`${window.location.pathname.match(/admin/) ? "": "root-page-container"}`}>
          <div className="app-container__header" style={{ display: `${window.location.pathname.match(/admin/) ? 'none' : 'flex'}`}}>
              <div className="flex-center">

                  <Menu mode="horizontal" theme="light" className="app-container__main-menu">
                      <Menu.Item style={{ border: "none"}}>
                          <Link to="/"><img alt="logo" src={locale == 'ru' ? logoMain : "/static/upload/assets/Logotype-Color-Eng.svg"} style={{ width: "190px", paddingBottom: 5}}/></Link>
                      </Menu.Item>

                      {this.state.nav.map( node => {
                          return (
                            <Menu.Item style={{ border: "none", display: this.state.locale == 'en' && node.parent.url == '/media' ? "none":"block"}} key={(Date.now() * Math.random())}>
                              <Popover trigger="hover" 
                                       placement="bottom"
                                       overlayStyle={{
                                           display: window.location.pathname.match(node.parent.url) ||  node.children.length <= 1  ? "none":"block"
                                       }}
                                       overlayClassName="link-popover-container" 
                                       content={node.children.map( child => (
                                        <Link key={child.url} 
                                        style={{margin:"10px 0", display: child.visible ? "none" : "block"}} 
                                        className="link-override" 
                                        to={node.parent.url + child.url}>
                                            {child['nav_name_'+this.state.locale]}
                                        </Link>                                      
                                        ))}>
                                  <Link style={{display: node.parent.visible ? "none" : "block"}} className={"app-container__main-menu__link " + (window.location.pathname.match(node.parent.url) ? "link-override-active" : "")} to={node.parent.url} onClick={this.toggleMenu} >{node.parent['nav_name_'+this.state.locale]}</Link>
                              </Popover>
                              </Menu.Item>
                              )
                      })}
                  </Menu>

                  <div className="nav__mobile" >
                      <img onClick={this.toggleMenu} src={ this.state.mmstate ? barClosed :barOpened} style={{width: 30, paddingBottom: 4, marginLeft:10}}/>
                      <div ref={ this.state.mmref }
                           className={ this.state.mmstate
                               ? 'nav__mobile__curtain opened'
                               : 'nav__mobile__curtain closed' }>
                          {this.state.nav.map( node => {
                              return (
                                  <Link style={{display: node.parent.visible ? "none" : "block"}} key={node.parent.url} className="link-override" onClick={this.toggleMenu} to={node.parent.url}>{node.parent['nav_name_'+this.state.locale]}</Link>
                              )
                          })}
                          <div className="flex-center" style={{display:'flex', justifyContent:'center'}}>
                              { this.state.socials.map( item => {
                                  return (
                                      <a href={item.link} key={item.id} dangerouslySetInnerHTML={{
                                          __html: item.svg ? item.svg : <img src={item.img} className="social-icon" alt='social'/>
                                      }}>
                                      </a>
                                  )
                              })}
                          </div>
                      </div>
                      <Link to='/' className="nav__mobile__logo"><img style={{width: 190}} alt="logo mobile" src={locale == 'ru' ? logoMain : "/static/upload/assets/Logotype-Color-Eng.svg"}/></Link>

                  </div>

                  <div className="lang-social-block">
                      <Select className="select-locale" defaultValue={this.state.locale} style={{ width:80, marginLeft: "auto", marginRight:10 }} onChange={this.handleSelect.bind(this)}>
                          <Option value="en">en</Option>
                          <Option value="ru">рус</Option>
                      </Select>
                      <div className="flex-center social">
                          { this.state.socials.map( item => {
                              return (
                                <a href={item.link} key={item.id} dangerouslySetInnerHTML={{
                                    __html: item.svg ? item.svg : <img src={item.img} className="social-icon" alt='social'/>
                                }}>
                                </a>
                              )
                          })}
                      </div>
                  </div>
              </div>
          </div>
          <Layout className={ window.location.pathname.match(/admin/) ? 'dashboard-container' : 'flex-center'}>
              <Content className={ window.location.pathname.match(/admin/) ? '' : 'page-container'}>
                  <Switch>
                      <Route exact path="/" render={ (props) => <Main locale={this.state.locale}/>} />
                      {this.state.nav.map( node => {
                          return (<Route path={node.parent.url} key={(Date.now() * Math.random())} render={(props) =>
                              <BlankPage locale={this.state.locale}
                                         {...props}
                                         parent={node.parent}
                                         children={node.children}/>}/>)
                      })}
                      <Route path="/admin" component={Dashboard}/>
                      <Route path="*" component={Main}/>
                  </Switch>
              </Content>
          </Layout>

          <Footer className="app-container__footer" style={{display: `${window.location.pathname.match(/admin/) ? 'none' : 'flex'}` }}>
              <img className="footer__logo_tk" alt="logo-taltek" src={locale == 'ru' ? logoWhite : "/static/upload/assets/Logotype-White-Eng.svg"} />
              <h2 className="footer__tk-txt" style={{margin:0}} dangerouslySetInnerHTML={{__html: "&copy 2018 ГК ТАЛТЕК"}}></h2>
              { locale === 'ru' ? 
              <div id="kodyagency">Сделано в <a href="http://kody.agency" target="_blank">kody<span>agency</span></a></div>
              : <div id="kodyagency">Site by <a href="http://kody.agency" target="_blank">kody<span>agency</span></a></div>
              }
          </Footer>

      </Layout>

    );
  }
}

export default App;
