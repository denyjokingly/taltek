$(document).ready(function() {

	//Loader Section
	$("#loadPage img").css({
		"-webkit-transform": "translateY(0)",
		"opacity": 1,
		"left": 0.5*$(window).innerWidth() - 0.5*$('#loadPage img').innerWidth(),
		"top": 0.5*$(window).innerHeight() - 0.5*$('#loadPage img').innerHeight()
	});

	$("#loadPage svg").css({
		"-webkit-transform": "translateY(0)",
		"opacity": 1
	});

  });
