$(document).ready(function(){

    $("header").css({"-webkit-transform": "translateY(-150px)", "opacity": 0 });
    $("#controlSlideBigSlider").css({"-webkit-transform": "translateY(150px)", "opacity": 0 });

    //Hide All start go Slider
    $('.slideBigSlider').addClass('hiddenBigSlider');
    $('.linkControlSlideBigSlider:first-child').addClass('active');

    //Setup Data control for Slides
    $('.slideBigSlider').each(function(i,elem) {
      $(this).attr("data-control-slide", i);
      $(this).attr("id", 'slideBigSlider-' + i);
    });

    //Setup Data control for Link
    $('.linkControlSlideBigSlider').each(function(i,elem) {
      $(this).attr("data-control-slide", i);
      $(this).attr("id", 'linkControlSlideBigSlider-' + i);
    });

    //End Loading Page
    $(window).on('load', function() {

      setTimeout(function(){

        $("#loadPage img").css({"opacity": 0});
        $("#loadPage svg").css({"opacity": 0});
        $("#loadPage").fadeOut(500);

        $("header").addClass('load');
        $("#controlSlideBigSlider").addClass('load');

        //Setup add Style Slider
        $('.slideBigSlider:first-child').removeClass('hiddenBigSlider').addClass('showBigSlider');
        $('.btSlideBigSlider a').addClass('jumpToPage');

        $("#linkControlSlideBigSlider").after("<div class='lineLinkLoader'>After</div>");

        widthFirstLink = $('.linkControlSlideBigSlider:first-child').width();

        $('.lineLinkLoader').css({
          'bottom': '50px',
          '-webkit-transition': 'width 30s linear',
          '-moz-transition': 'width 30s linear',
          '-o-transition': 'width 30s linear',
          'transition': 'width 30s linear',
          '-webkit-transform': 'translateX(50px)',
          'width': widthFirstLink
        });

        if ( $(window).width() < 1024) { $('.lineLinkLoader').css({ 'bottom': 2, '-webkit-transform': 'translateX(2px)' }); }

      }, 50);

    });

    //Function Autoplay
    function runSlide() {
      currentSlide = $('.showBigSlider').attr('data-control-slide');
      nextSlide = parseInt(currentSlide) + 1;
      lastSlide = $("#linkControlSlideBigSlider div:last-child").attr('data-control-slide');

      if ( nextSlide > parseInt(lastSlide) ) {
        var nextSlide = 0;
      }

      posActiveLink = $('#linkControlSlideBigSlider-' + nextSlide ).position();
      widthActiveLink = $('#linkControlSlideBigSlider-' + nextSlide ).width();
      xPosActiveLink = posActiveLink.left + 20;

      $('.slideBigSlider').addClass('hiddenBigSlider').removeClass('showBigSlider');

      $('#slideBigSlider-' + nextSlide ).removeClass('hiddenBigSlider').addClass('showBigSlider');
      $('#slideBigSlider-' + currentSlide ).addClass('run');

      $('.linkControlSlideBigSlider' ).removeClass('active');
      $('#linkControlSlideBigSlider-' + nextSlide ).addClass('active');

      $('.lineLinkLoader').css({
        '-webkit-transition': 'width 30s linear',
        '-moz-transition': 'width 30s linear',
        '-o-transition': 'width 30s linear',
        'transition': 'width 30s linear',
        '-webkit-transform': 'translateX('+ xPosActiveLink +'px)',
        'width': widthActiveLink
      });

      if ( $(window).width() < 1024) { $('.lineLinkLoader').css({ '-webkit-transform': 'translateX('+ posActiveLink.left +'px)' }); }

    };

    //Function Autoplay
    function prevSlide() {
      currentSlide = $('.showBigSlider').attr('data-control-slide');
      nextSlide = parseInt(currentSlide) - 1;
      lastSlide = $("#linkControlSlideBigSlider div:last-child").attr('data-control-slide');

      if ( nextSlide < 0 ) {
        var nextSlide = lastSlide;
      }

      posActiveLink = $('#linkControlSlideBigSlider-' + nextSlide ).position();
      widthActiveLink = $('#linkControlSlideBigSlider-' + nextSlide ).width();
      xPosActiveLink = posActiveLink.left + 20;

      $('.slideBigSlider').addClass('hiddenBigSlider').removeClass('showBigSlider');

      $('#slideBigSlider-' + nextSlide ).removeClass('hiddenBigSlider').addClass('showBigSlider');
      $('#slideBigSlider-' + currentSlide ).addClass('run');

      $('.linkControlSlideBigSlider' ).removeClass('active');
      $('#linkControlSlideBigSlider-' + nextSlide ).addClass('active');

      $('.lineLinkLoader').css({
        '-webkit-transition': 'width 30s linear',
        '-moz-transition': 'width 30s linear',
        '-o-transition': 'width 30s linear',
        'transition': 'width 30s linear',
        '-webkit-transform': 'translateX('+ xPosActiveLink +'px)',
        'width': widthActiveLink
      });

      if ( $(window).width() < 1024) { $('.lineLinkLoader').css({ '-webkit-transform': 'translateX('+ posActiveLink.left +'px)' }); }

    };

    //Interval Autoplay Slide
    function autoPlaySlide(){

      interval = setInterval(function(){
        $('.lineLinkLoader').css({
          '-webkit-transition': 'none',
          '-moz-transition': 'none',
          '-o-transition': 'none',
          'transition': 'none',
          'width': 0
        });
        runSlide();
      }, 31000);

    };

    //Start Autoplay Slide
    autoPlaySlide();

    //Action Control Link Slide
    $('.linkControlSlideBigSlider').click(function () {

            clearInterval(interval);

            $('.lineLinkLoader').css({
              '-webkit-transition': 'none',
              '-moz-transition': 'none',
              '-o-transition': 'none',
              'transition': 'none',
              'width': 0
            });

            currentSlide = $('.showBigSlider').attr('data-control-slide');
            nextSlide = $(this).attr('data-control-slide');
            lastSlide = $("#linkControlSlideBigSlider div:last-child").attr('data-control-slide');

            if ( nextSlide > parseInt(lastSlide) ) {
              var nextSlide = 0;
            }

            posActiveLink = $('#linkControlSlideBigSlider-' + nextSlide ).position();
            widthActiveLink = $('#linkControlSlideBigSlider-' + nextSlide ).width();
            xPosActiveLink = posActiveLink.left + 20;

            $('.slideBigSlider').addClass('hiddenBigSlider').removeClass('showBigSlider');

            $('#slideBigSlider-' + nextSlide ).removeClass('hiddenBigSlider').addClass('showBigSlider');
            $('#slideBigSlider-' + currentSlide ).addClass('run');

            $('.linkControlSlideBigSlider' ).removeClass('active');
            $('#linkControlSlideBigSlider-' + nextSlide ).addClass('active');

            $('.lineLinkLoader').css({
              '-webkit-transition': 'width 30s linear',
              '-moz-transition': 'width 30s linear',
              '-o-transition': 'width 30s linear',
              'transition': 'width 30s linear',
              '-webkit-transform': 'translateX('+ xPosActiveLink +'px)',
              'width': widthActiveLink
            });

            if ( $(window).width() < 1024) { $('.lineLinkLoader').css({ '-webkit-transform': 'translateX('+ posActiveLink.left +'px)' }); }

            autoPlaySlide();

      });

      //Action Next Arrows Slide
      $('#navControlSlideBigSlider .nextNavControl').click(function () {

            clearInterval(interval);

            $('.lineLinkLoader').css({
              '-webkit-transition': 'none',
              '-moz-transition': 'none',
              '-o-transition': 'none',
              'transition': 'none',
              'width': 0
            });

            runSlide();
            autoPlaySlide();

      });

      //Action Prev Arrows Slide
      $('#navControlSlideBigSlider .prevNavControl').click(function () {

            clearInterval(interval);

            $('.lineLinkLoader').css({
              '-webkit-transition': 'none',
              '-moz-transition': 'none',
              '-o-transition': 'none',
              'transition': 'none',
              'width': 0
            });

            prevSlide();
            autoPlaySlide();

        });

});
