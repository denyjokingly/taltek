import React, { Component } from 'react';
import NewsPage from './NewsPage';

export default class Main extends Component{

    componentDidMount(){
        document.querySelector('title').innerHTML = this.props.page['title_'+this.props.locale] + ( this.props.locale === 'ru'? " &mdash; ТАЛТЕК" : " &mdash TALTEK");
        document.querySelector('body').classList = "";
        document.querySelector('body').classList.add(this.props.page.url.slice(1));
        window.scrollTo(0,0);
        const $ = window.$;
        const sliders = $('.slick-slider');
        sliders.map( (idx, slider) => {
           const config = $(slider).data().slick;
           const sl = $(slider).slick(config);

           const counter = sl.get(0).classList[1];
           const $counter = ($('.counter-'+counter));
           $counter.html(`1\/${sl.get(0).slick.slideCount}`);

           sl.on('afterChange', (evt, slick, currentSlide) => {
               $counter.html(`${currentSlide + 1}\/${slick.slideCount}`)
           })
        });

        if(this.props.page.script){
            const script = document.createElement('script');
            script.type='text/javascript';
            script.setAttribute('data-id',this.props.page.id);
            let shouldPass = false;
            Array.prototype.slice.call(document.getElementsByTagName('script')).map( script => {
                if(script.getAttribute('data-id') == this.props.page.id ) {
                    document.getElementsByTagName('body')[0].removeChild(script);
                };
            });           

            document.getElementsByTagName('body')[0].appendChild(script);
            script.innerHTML = this.props.page.script; 
        }

    }

    getView = () => {
        const path = this.props.match.path.split('/').filter( item => item !== "");
        if(path[0] !== 'media'){
            return (
                <div className="page-container"
                     dangerouslySetInnerHTML={{__html: this.props.page['text_'+this.props.locale]}}>
                </div>
            )
        } else {
            return (
                <div className={(path[1]==='video'? "page-video":"")+" page-container"}>
                    <NewsPage locale={this.props.locale} type={path[1]}/>
                </div>
            )
        }
    };

    render(){
        return (
            <div className="page-container">
                <h1 className="page-heading">{this.props.page['title_'+this.props.locale]}</h1>
                {this.getView()}
            </div>

        )
    }
}