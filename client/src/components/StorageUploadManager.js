import React, {Component} from 'react';
import { Avatar, List, Upload, message, Button, Icon, Input, Popconfirm } from 'antd';
import {Link} from 'react-router-dom';
import {CopyToClipboard} from 'react-copy-to-clipboard';

const props = {
    name: 'uploadFile',
    action: '/api/upload/asset',
    onChange(info) {
        if (info.file.status !== 'uploading') {
            console.log(info.file, info.fileList);
        }
        if (info.file.status === 'done') {
            message.success(`Файл ${info.file.name} успешно загружен`);
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} не смог быть загружен.`);
        }
    },
}

const success = () => {
    message.success('Файл скопирован в буффер обмена');
};

export default class StorageUploadManager extends Component {

    state = {
        docs: []
    };

    getDocs(){
        fetch('/api/upload/asset')
            .then( res => res.json())
            .then( docs => {
                new Promise(resolve => {
                    docs.map( (doc,idx) => {
                        this.getImageDimensions(doc)
                        .then( dimensions => {
                            console.log(dimensions);
                            doc.dimensions = dimensions
                            return idx;
                        })
                        .then( idx => {
                            console.log(idx);
                            if(idx == docs.length -1 ) return resolve(docs); 
                        })
                    })
                }).then( docs => this.setState({docs: docs}) );
            })
            .catch( err => console.log(err));
    }

    componentDidMount(){
        this.getDocs();
    }

    removeFile = file => {
        fetch('/api/upload/asset/'+file.name, {
            method: 'DELETE',
        })
            .then( () => this.getDocs() )
            .catch( err => console.log(err));
    };

    downloadLink = () => {
        const link = this.state.downloadLink;

        fetch('/api/upload/remote/asset', {
            method:"POST",
            headers: {
                "Content-type":"application/json"
            },
            body: JSON.stringify({link: link})
        })
            .then( ()=> {
                window.location.reload();
            })
            .catch( err => console.log(err));
    }

    getImageDimensions = item => {
        console.log(item.file);
        const img = new Image();
        let loaded = false;

        img.addEventListener('load', (e)=> {
            loaded = true;
        });
        img.addEventListener('error', ()=> {
            loaded = true;
        })

        img.src = window.location.origin + '/' + item.path;

        return new Promise( resolve => {
            const ticker = setInterval( () => {
                if(loaded){
                    clearInterval(ticker);
                    resolve(img.width === 0 ? "N/A" : `${img.naturalWidth}x${img.naturalHeight}`);
                }
            }, 1000);
        });
    }

    render() {
        return (
            <div className="page-container">
                <h1>Файлы :</h1>
                
                <div style={{ display: "flex", justifyContent: "center"}}>
                    <Input onChange={(e)=> this.setState({downloadLink: e.target.value})} placeholder="Скачать файл по внешней ссылке"/>
                    <Button onClick={this.downloadLink} type="primary" style={{marginLeft: 15}}><Icon type="cloud-download"/></Button>
                </div>
                <Upload {...props} showUploadList={false} onChange={this.getDocs.bind(this)}>
                    <Button style={{margin: "10px auto"}}>
                        <Icon type="upload" /> Загрузить файл
                    </Button>
                </Upload>
                <List
                    itemLayout="horizontal"
                    dataSource={this.state.docs}
                    pagination={{
                        onChange: (page) => {
                            // console.log(page);
                        },
                        pageSize: 30,
                    }}
                    renderItem={ item => (
                        <List.Item
                            actions={[<Popconfirm title="Подтвердите действие" 
                            okText="Да" 
                            cancelText="Нет" 
                            onCancel={ () => console.log('cancel')} 
                            onConfirm={this.removeFile.bind(this, item)}><a><Icon type="delete"/></a></Popconfirm>,
                            <CopyToClipboard text={item.path.slice(1)} onCopy={()=> success()}>
                                <Icon type="copy" />
                            </CopyToClipboard>
                            ]}>
                            <List.Item.Meta
                                avatar={<Avatar src={window.location.origin+"/"+item.path}/>}
                                title={<Link to={item.path}>{item.name}</Link>}
                                description={"Создан : "+new Date(item.created).toLocaleDateString()+" / Размер : "+item.size}/>
                                <span>{"Разрешение : " + item.dimensions }</span>
                        </List.Item>
                    )}/>
            </div>
        )
        return null;
    }
}