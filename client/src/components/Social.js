import React, {Component} from 'react';
import {Form, Input, Button, message, Row, Col} from 'antd';
const FormItem = Form.Item;
const success = () => {
    message.success('Изменения успешно применены');
};
const error = () => {
    message.error('Произошла ошибка');
};
const CustomizedForm = Form.create({
    onFieldsChange(props, changedFields){
        props.onChange(changedFields);
    },
    mapPropsToFields(props) {
        return {
            img: Form.createFormField({
                ...props.img,
                value: props.img.value,
            }),
            link: Form.createFormField({
                ...props.link,
                value: props.link.value
            }),
            title: Form.createFormField({
                ...props.title,
                value: props.title.value
            }),
        };
    },
    onValuesChange(_, values) {
        //console.log(values);
    },
})((props) => {
    const { getFieldDecorator } = props.form;
    return (
        <Row>
            <Col span={12} offset={1}>
                <Form onSubmit={props.onSubmit}>
                    <FormItem
                        label="Ссылка на картинку">
                        {getFieldDecorator('img',{
                            rules: [{required: false, message: "Поле не должно быть пустым"}]
                        })(<Input />)}
                    </FormItem>
                    <FormItem
                        label="Ссылка на страницу">
                        {getFieldDecorator('link',{
                            rules: [{required: false, message: "Поле не должно быть пустым"}]
                        })(<Input />)}
                    </FormItem>
                    <FormItem
                        label="Заголовок">
                        {getFieldDecorator('title',{
                            rules: [{required: false, message: "Поле не должно быть пустым"}],
                        })(<Input />)}
                    </FormItem>
                    <Button type="primary" htmlType="submit" disabled={
                        props.title.value == '' ||
                        props.img.value == '' ||
                        props.link.value == ''
                    }>Сохранить</Button>
                    {!props.isNew && <Button type="danger" onClick={props.deletePage} style={{marginLeft: "5px"}}>Удалить</Button>}
                </Form>
            </Col>
        </Row>
    );
});

export default class Social extends Component {
    state = {
        isNew: true,
        fields: {
            img: {
                value: ''
            },
            link: {
                value: ''
            },
            title: {
                value: ''
            },
        },
        socials: []
    };

    componentDidMount() {
        this.getSocials();
    }
    handleFormChange = (changedFields) => {
        this.setState(({ fields }) => ({
            fields: { ...fields, ...changedFields },
        }));
    };

    deleteSocial() {
        const { match: { params } } = this.props;
        if( !params.socialId ) return;
        fetch(`/api/page/${params.socialId}`,{
            method: 'DELETE',
        })
            .then( () => this.props.history.push('/admin/social'))
            .catch( err => console.log(err));
    }

    getSocials() {
        const { match: { params } } = this.props;
        if( !params.socialId ) return;

        console.log(params.socialId);
        fetch('/api/social/'+params.socialId)
            .then( res => res.json())
            .then( social => this.patchFields(social))
            .catch( err => console.log(err))
    }

    patchFields(card) {

        const updatedFields = Object.assign({}, this.state.fields);
        for(let prop in card){
            if(updatedFields[prop]) updatedFields[prop].value = card[prop];
        }
        this.setState((state) => ({ fields: updatedFields, isNew: false }));
    }

    sendUpdate = (e) => {
        e.persist();
        e.preventDefault();

        let cardToUpdate = {
            img: this.state.fields.img.value,
            link: this.state.fields.link.value,
            title: this.state.fields.title.value,
        };

        const { match: { params } } = this.props;
        const route = this.state.isNew ? '' : `${params.socialId}`;

        fetch(`/api/social/${route}`, {
            method: 'POST',
            cache: 'no-cache',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify(cardToUpdate)
        })
            .then( res => res.json() )
            .then( card => {
                success();
                const {socials} = this.state;

                socials.map( s => {
                    if(s.id === card.id) {
                        s = Object.assign({}, card);
                    }
                    return void 0;
                });
                this.setState({socials: socials});
                return void 0;
            })
            .catch( err=> {
                error();
                console.log(err);
                this.getSocials();
            })
    };

    render() {
        const fields = this.state.fields;

        return (
            <CustomizedForm {...fields}
                            onChange={this.handleFormChange}
                            deleteSocial={this.deleteSocial.bind(this)}
                            isNew={this.state.isNew}
                            onSubmit={this.sendUpdate}/>
        )
    }
}