import React, { Component } from "react";
import { Form, Input, Button, Tabs, Icon } from "antd";
import { message } from "antd/lib/index";

const TabPane = Tabs.TabPane;

const success = () => {
  message.success("Изменения успешно применены");
};

export default class Slider extends Component {
  constructor(props) {
    super(props);

    this.newTabIndex = 0;
    this.state = {
      content: {
        slides: [],
        id: "",
        name: ""
      },
      activeKey: "0"
    };
  }

  componentDidMount() {
    const {
      match: { params }
    } = this.props;

    fetch("/api/slider/" + params.sliderId)
      .then(res => res.json())
      .then(slider => {
        this.setState({ ...slider });
        console.log(this.state);
      });
  }

  onChange = e => {
    this.setState({
      value: e.target.value
    });
  };

  onTabChange = activeKey => {
    this.setState({ activeKey: activeKey });
  };

  onTabEdit = (targetKey, action) => {
    this[action](targetKey);
  };

  add = () => {
    const content = this.state.content;

    content.slides.push({
      url: "",
      text_ru: "",
      text_en: "",
      nav_top_text_ru: "",
      nav_top_text_en: "",
      nav_bot_text_ru: "",
      nav_bot_text_en: "",
      img: ""
    });
    const activeKey = `${content.slides.length - 1}`;

    this.setState({ content, activeKey });
  };

  remove = targetKey => {
    let activeKey = this.state.activeKey;

    const content = this.state.content;
    if (targetKey == 0) activeKey = "0";
    else if (activeKey === targetKey) {
      activeKey = `${targetKey - 1}`;
      console.log(activeKey);
    }
    content.slides.splice(targetKey, 1);

    this.setState({ content, activeKey });
  };

  saveChanges() {
    const { id, content } = this.state;

    fetch("/api/slider/" + id, {
      method: "POST",
      cache: "no-cache",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(content)
    })
      .then(res => res.json())
      .then(slider => this.setState({ ...slider }))
      .then(() => success())
      .catch(err => console.log(err));
  }

  formChanged = e => {
    const { content } = this.state;
    const [key, slideIdx] = e.target.name.split(":");
    content.slides[slideIdx][key] = e.target.value;

    this.setState(() => ({ content: content }));
  };

  swapExtToLocal = item => {
    console.log(item.img);
    fetch("/api/upload/asset", {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify({ link: item.img })
    })
      .then(res => res.json())
      .then(({ file }) => {
        const { content } = this.state;
        content.slides[content.slides.indexOf(item)].img = file;

        this.setState({ content: content });
        this.saveChanges();
        window.location.reload();
      });
  };

  render() {
    return (
      <div className="pages-list-wrapper">
        <h1>Параметры:</h1>
        <Form onChange={this.formChanged}>
          <Form.Item>
            <Tabs
              hideAdd
              onChange={this.onTabChange}
              activeKey={this.state.activeKey}
              type="editable-card"
              onEdit={this.onTabEdit}
            >
              {this.state.content.slides.map((slide, idx) => (
                <TabPane tab={`Слайд ${idx + 1}`} key={idx}>
                  <Form.Item label="Ссылка на страницу">
                    <Input name={`url:${idx}`} defaultValue={slide.url} />
                  </Form.Item>
                  <Form.Item label="Текст слайда (русский)">
                    <Input
                      name={`text_ru:${idx}`}
                      defaultValue={slide.text_ru}
                    />
                  </Form.Item>
                  <Form.Item label="Текст слайда (английский)">
                    <Input
                      name={`text_en:${idx}`}
                      defaultValue={slide.text_en}
                    />
                  </Form.Item>
                  <Form.Item label="Верхняя ссылка навигации (русский)">
                    <Input
                      name={`nav_top_text_ru:${idx}`}
                      defaultValue={slide.nav_top_text_ru}
                    />
                  </Form.Item>
                  <Form.Item label="Верхняя ссылка навигации (английский)">
                    <Input
                      name={`nav_top_text_en:${idx}`}
                      defaultValue={slide.nav_top_text_en}
                    />
                  </Form.Item>
                  <Form.Item label="Нижняя ссылка навигации (русский)">
                    <Input
                      name={`nav_bot_text_ru:${idx}`}
                      defaultValue={slide.nav_bot_text_ru}
                    />
                  </Form.Item>
                  <Form.Item label="Нижняя ссылка навигации (английский)">
                    <Input
                      name={`nav_bot_text_en:${idx}`}
                      defaultValue={slide.nav_bot_text_en}
                    />
                  </Form.Item>
                  <Form.Item label="Ссылка на картинку">
                    <div style={{ display: "flex", justifyContent: "center" }}>
                      <Input name={`img:${idx}`} defaultValue={slide.img} />
                      <Button
                        icon="cloud-download"
                        style={{ marginLeft: 50 }}
                        disabled={slide.img.search("http") == -1 ? true : false}
                        onClick={this.swapExtToLocal.bind(this, slide)}
                      >
                        Сохранить
                      </Button>
                    </div>
                  </Form.Item>
                </TabPane>
              ))}
            </Tabs>
          </Form.Item>
        </Form>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "flex-end"
          }}
        >
          <div style={{ margin: 5 }}>
            <Button type="dashed" onClick={this.add}>
              Добавить слайд
            </Button>
          </div>

          <div style={{ margin: 5 }}>
            <Button type="primary" onClick={this.saveChanges.bind(this)}>
              Сохранить изменения
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
