import React, {Component} from 'react';
import { Form, Input, Button, message, Row, Col } from 'antd';
import Template from './Template';

const FormItem = Form.Item;
const { TextArea } = Input;
const success = () => {
    message.success('Изменения успешно применены');
};
const error = () => {
    message.error('Произошла ошибка');
};
const CustomizedForm = Form.create({
    onFieldsChange(props, changedFields){
        props.onChange(changedFields);
    },
    mapPropsToFields(props) {
        return {
            title_ru: Form.createFormField({
                ...props.title_ru,
                value: props.title_ru.value,
            }),
            title_en: Form.createFormField({
                ...props.title_en,
                value: props.title_en.value
            }),
            className: Form.createFormField({
                ...props.className,
                value: props.className.value
            }),
            link: Form.createFormField({
                ...props.link,
                value: props.link.value
            }),
            text_ru: Form.createFormField({
                ...props.text_ru,
                value: props.text_ru.value
            }),
            text_en: Form.createFormField({
                ...props.text_en,
                value: props.text_en.value
            }),
        };
    },
    onValuesChange(_, values) {
        //console.log(values);
    },
})((props) => {
    const { getFieldDecorator } = props.form;
    return (
        <Row>
            <Col span={12} offset={1}>
                <Form onSubmit={props.onSubmit}>
                    <FormItem
                        label="Заголовок (английский)">
                        {getFieldDecorator('title_en',{
                            rules: [{required: true, message: "Поле не должно быть пустым"}]
                        })(<Input />)}
                    </FormItem>
                    <FormItem
                        label="Заголовок (русский)">
                        {getFieldDecorator('title_ru',{
                            rules: [{required: true, message: "Поле не должно быть пустым"}]
                        })(<Input />)}
                    </FormItem>
                    <FormItem
                        label="Имя класса маркера">
                        {getFieldDecorator('className',{
                            rules: [{required: true, message: "Поле не должно быть пустым"}]
                        })(<Input />)}
                    </FormItem>
                    <FormItem
                        label="Ссылка">
                        {getFieldDecorator('link',{
                            rules: [{required: true, message: "Поле не должно быть пустым"}]
                        })(<Input />)}
                    </FormItem>
                    <FormItem
                        label="Текст (русский)">
                        {getFieldDecorator('text_ru',{
                            rules: [{required: true, message: "Поле не должно быть пустым"}]
                        })(<TextArea />)}
                    </FormItem>
                    <FormItem
                        label="Текст (английский)">
                        {getFieldDecorator('text_en',{
                            rules: [{required: true, message: "Поле не должно быть пустым"}]
                        })(<TextArea />)}
                    </FormItem>
                    <Button type="primary" htmlType="submit"
                            disabled={
                                props.title_ru === '' ||
                                props.text_ru === '' ||
                                props.link === ''
                            }
                    >Сохранить</Button>
                    {!props.isNew && <Button type="danger" onClick={props.deletePage} style={{marginLeft: "5px"}}>Удалить</Button>}
                </Form>
            </Col>
        </Row>
    );
});

message.config({
    maxCount: 1,
    duration:2,
});

export default class Page extends Component {
    constructor() {
        super();
        this.state = {
            isNew : true,
            fields: {
                title_ru: {
                    value: ''
                },
                title_en: {
                    value: ''
                },
                link: {
                    value: ''
                },
                className: {
                    value: ''
                },
                text_ru: {
                    value: ''
                },
                text_en: {
                    value: ''
                }
            },
            content: []
        };
    }

    componentDidMount() {
        this.getMarker();
    }

    getMarker() {
        const { match: { params } } = this.props;
        if( !params.markerId ) return;
        fetch(`/api/marker/${params.markerId}`)
            .then( res => res.json())
            .then( marker => this.patchFields(marker));
    }

    deleteMarker(){
        const { match: { params } } = this.props;
        if( !params.markerId ) return;
        fetch(`/api/marker/${params.markerId}`,{
            method: 'DELETE',
        })
        .then( () => this.props.history.push('/admin/mapeditor'))
        .catch( err => console.log(err));

    }

    handleFormChange = (changedFields) => {
        this.setState(({ fields }) => ({
            fields: { ...fields, ...changedFields },
        }));
    };

    patchFields(page) {
        const updatedFields = Object.assign({}, this.state.fields);
        for(let prop in page){
            if(updatedFields[prop]) updatedFields[prop].value = page[prop];
        }
        this.setState((state) => ({isNew: false, fields: updatedFields }));
        console.log(this.state.fields);
    }

    
    sendUpdate = (e) => {
        e.persist();
        e.preventDefault();

        let markerToUpdate = {
            title_ru: this.state.fields.title_ru.value,
            title_en: this.state.fields.title_en.value,
            className: this.state.fields.className.value,
            link: this.state.fields.link.value,
            text_ru: this.state.fields.text_ru.value,
            text_en: this.state.fields.text_en.value,
        };
        const { match: { params } } = this.props;
        const route = this.state.isNew ? '' : `${params.markerId}`;

        fetch(`/api/marker/${route}`, {
            method: 'POST',
            cache: 'no-cache',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify(markerToUpdate)
        })
        .then( res => res.json() )
        .then( marker => {
            success();
            this.patchFields(marker)
            this.props.history.push('/admin/edit/marker/'+marker.id)
        })
        .catch( err=> {
            error();
            console.log(err);
            this.getMarker();
        })
    };

    render(){
        const fields = this.state.fields;

        return (
            <div className="page-container">
                <CustomizedForm {...fields} 
                                        onChange={this.handleFormChange} 
                                        deletePage={this.deleteMarker.bind(this)} 
                                        isNew={this.state.isNew} 
                                        onSubmit={this.sendUpdate}/>
            </div>
        )
    }
}