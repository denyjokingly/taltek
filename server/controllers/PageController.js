const Page = require("../models").Page;
const fs = require("fs");

module.exports = {
  create(req, res) {
    return Page.create({
      title_ru: req.body.title_ru,
      title_en: req.body.title_en,
      nav_name_en: req.body.nav_name_en,
      nav_name_ru: req.body.nav_name_ru,
      url: req.body.url,
      text_ru: req.body.text_ru,
      text_en: req.body.text_en,
      is_main: false,
      visible: false,
      script: req.body.script
    })
      .then(page => res.status(200).send(page))
      .catch(err => res.status(400).send(err));
  },
  list(req, res) {
    return Page.findAll().then(pages => res.status(200).send(pages));
  },
  retrieve(req, res) {
    return Page.findById(req.params.pageId)
      .then(page => {
        if (!page) {
          return res.status(404).send({
            message: "страница не найдена"
          });
        }
        return res.status(200).send(page);
      })
      .catch(err => res.status(400).send(err));
  },
  retrieveByName(req, res) {
    const endpage = req.query.name.split("/");
    return Page.findAll()
      .then(pages => {
        const result = pages.filter(page => {
          return page.url === "/" + endpage[endpage.length - 1];
        });
        if (!result.length) {
          return res.status(404).send({
            message: "страница не найдена"
          });
        }
        return res.status(200).send(result[0]);
      })
      .catch(err => res.status(400).send(err));
  },
  update(req, res) {
    return Page.findById(req.params.pageId)
      .then(page => {
        if (!page) {
          console.log("Page not found!");
          return res.status(404).send({
            message: "todo not found"
          });
        }

        return page
          .update(req.body, {
            fields: Object.keys(req.body)
          })
          .then(() => res.status(200).send(page))
          .catch(err => res.status(400).send(err));
      })
      .catch(err => res.status(400).send(err));
  },
  destroy(req, res) {
    return Page.findById(req.params.pageId)
      .then(page => {
        if (!page) {
          return res.status(400).send({ message: "Страница не найдена" });
        }
        return page
          .destroy()
          .then(() => res.status(204).send())
          .catch(err => res.status(400).send(err));
      })
      .catch(err => res.status(400).send(err));
  },
  toggleMain(req, res) {
    return Page.findById(req.params.pageId).then(page => {
      if (!page) {
        return res.status(400).send({ message: "Страница не найдена" });
      }

      return page
        .update({
          is_main: req.body.is_main
        })
        .then(page => res.status(200).send(page))
        .catch(err => res.status(400).send(err));
    });
  },
  toggleVisible(req, res) {
    return Page.findById(req.params.pageId).then(page => {
      if (!page) {
        return res.status(400).send({ message: "Страница не найдена" });
      }

      return page
        .update({
          visible: req.body.visible
        })
        .then(page => res.status(200).send(page))
        .catch(err => res.status(400).send(err));
    });
  },
  backup(req, res) {
    return Page.findAll().then(pages => {
      const path = "./client/build/static/PAGES_BACKUP_" + Date.now();
      try {
        fs.writeFileSync(path, JSON.stringify(pages));
      } catch (err) {
        console.log(err);
      }
    });
  }
};
