import React, {Component} from 'react';
import { Affix, Button, Icon, Divider, List, Table, Popconfirm } from 'antd';
import {Link} from 'react-router-dom';
import moment from 'moment';
import 'moment/locale/ru';
import './NavigationManager.css';

export default class CardsManager extends Component {
    state = {
        cards:[],
    };

    columns = [
        {
            title: "Название",
            dataIndex: 'title_ru',
            sorter: (a,b) => {
                return a.title_ru.length - b.title_ru.length
            }
        },
        {
            title: "Дата",
            dataIndex: 'news_date',
            render: (text, record)  => {
                return (moment(record.news_date).locale('ru').format("LL"))
            },
            sorter: (a,b) => moment(a.news_date) - moment(b.news_date),
        },
        {
            title: "Тип",
            dataIndex: 'tags',
            render: (text, record) => {
                switch (record.tags){
                    case 'news':
                        return 'Новость';
                    case 'video':
                        return 'Видео';
                    case 'pr':
                        return 'Пресс-релиз'
                }
            },
            sorter: (a,b) => a.tags.length - b.tags.length
        },
        {
            title: "Действия",
            dataIndex: "actions",
            render:  (text, record) => {
                return (
                    <div style={{width: 60, display: "flex", justifyContent:"space-around"}}>
                        <Link style={{ textDecoration: "none", color: "#a1a1a1"}} to={`/admin/edit/news/${record.id}`}><Icon type="edit"/></Link>
                        <Popconfirm title="Подтвердите действие" 
                                    okText="Да" 
                                    cancelText="Нет" 
                                    onCancel={ () => console.log('cancel')} 
                                    onConfirm={this.deleteCard.bind(this,`${record.id}`)}>
                                    <a style={{ textDecoration: "none", color: "#a1a1a1"}}><Icon type="close"/></a>
                                    </Popconfirm>
                    </div>
                )
            }
        }
    ];
    componentDidMount(){
        this.getCards();
    }

    getCards(){
        fetch('/api/card')
            .then( res => res.json())
            .then( cards => cards.filter( card => card.is_news) )
            .then( cards => this.setState({cards: cards}))
            .catch( err => console.log(err));
        console.log(this.state.cards);
    }

    deleteCard(itemId){
        fetch(`/api/card/${itemId}`,{
            method: "DELETE"
        })
            .then( () => this.getCards())
            .catch( err => console.log(err) );
    }


    render() {
        const {cards} = this.state;

        return (
            <div className="pages-list-wrapper">
                <h2>Список новостей:</h2>
                <Divider/>
                <Affix offsetTop={0} >
                    <Link to="/admin/create/news" className="pages-list__add-button">
                        <Button type="dashed" style={{width: '60%', marginBottom: 40}}>
                            <Icon type="plus"/> Добавить новостной элемент
                        </Button>
                    </Link>
                </Affix>

                {/*<List*/}
                    {/*pagination={{*/}
                        {/*onChange: (page) => {*/}
                            {/*console.log(page);*/}
                        {/*},*/}
                        {/*pageSize: 20,*/}
                    {/*}}*/}
                    {/*dataSource={cards}*/}
                    {/*renderItem={item => (*/}
                        {/*<List.Item*/}
                            {/*actions={[<Link style={{ textDecoration: "none", color: "#a1a1a1"}} to={`/dashboard/edit/news/${item.id}`}><Icon type="edit"/></Link>,*/}
                                {/*<a style={{ textDecoration: "none", color: "#a1a1a1"}}  onClick={this.deleteCard.bind(this, `${item.id}`)}><Icon type="close"/></a>]}>*/}
                                {/*<List.Item.Meta title={item.title_ru}>*/}
                                {/*</List.Item.Meta>*/}
                        {/*</List.Item>*/}
                    {/*)}*/}
                {/*/>*/}

                <Table columns={this.columns}
                       dataSource={cards}
                       pagination={{ pageSize: 12}} />

            </div>
        )
    }
}