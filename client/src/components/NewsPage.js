import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';
import NewsList from "./NewsList";
import NewsExpanded from "./NewsExpanded";

export default class NewsPage extends Component {
    render() {
        return (
            <Switch>
                <Route exact path="/media/:type" render={ props => <NewsList locale={this.props.locale} type={this.props.type}/>}/>
                <Route path="/media/news/:newsId" render={ (props) => <NewsExpanded locale={this.props.locale} {...props}/>}/>
                <Route path="/media/video/:newsId" render={ (props) => <NewsExpanded locale={this.props.locale} {...props}/>}/>
                <Route path="/media/pr/:newsId" render={ (props) => <NewsExpanded locale={this.props.locale} {...props}/>}/>
            </Switch>
        )
    }
}