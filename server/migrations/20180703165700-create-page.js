'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Pages', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title_en: {
        type: Sequelize.STRING
      },
      title_ru: {
        type: Sequelize.STRING
      },
      nav_name_en: {
        type: Sequelize.STRING
      },
      nav_name_ru: {
        type: Sequelize.STRING
      },
      url: {
        type: Sequelize.STRING
      },
      text_ru: {
        type: Sequelize.TEXT
      },
      text_en: {
          type: Sequelize.TEXT
      },
      is_main: {
        type: Sequelize.BOOLEAN
      },
      childPageId: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Pages');
  }
};