import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Link, Route} from 'react-router-dom';
import Draggable from 'react-draggable';
import SubPage from './SubPage';
import './SubPage.css';
import GeographySection from './GeographySection';

export default class Main extends Component{

    componentDidMount() {
        window.scrollTo(0,0);
        if( this.props.children.length > 0 && this.props.location.pathname === this.props.parent.url){
            this.props.history.push(this.props.parent.url + this.props.children[0].url);
        }

        const gc = document.querySelector('.geo-component');
        if(gc){
            ReactDOM.render(<GeographySection locale={this.props.locale}/>, gc);
        }
    }

    render(){
        return (
            <div className="page-container">
                <div className="page-nav">
                    <Draggable
                        axis="x"
                        bounds="parent">
                    <div className="page-nav__links">
                            { this.props.children.length > 1 && this.props.children.map( page => {
                                const {locale} = this.props;
                                return <Link style={{display: page.visible ? "none" : ""}}  className={this.props.location.pathname.match(this.props.parent.url + page.url) ?
                                    "subpage__link-active":"subpage__link"} key={page.id} to={this.props.parent.url + page.url}>{page['nav_name_'+locale]}</Link>
                            })}
                    </div>
                    </Draggable>
                </div>
                <div className="page-container">
                    { this.props.children.map( page => {
                        return <Route className="page-container" path={this.props.parent.url + page.url} key={(Date.now() * Math.random())} render={(props) => <SubPage {...props} className="subpage__content" locale={this.props.locale} page={page}/>} />
                    })}
                </div>
            </div>

        )
    }
}