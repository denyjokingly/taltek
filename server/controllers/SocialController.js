const Social = require('../models').Social;

module.exports = {
    create(req, res) {
        return Social
            .create({
                title: req.body.title,
                img: req.body.img,
                link: req.body.link
            })
            .then( social => res.status(200).send(social))
            .catch( err => res.status(400).send(err))
    },
    list(req, res) {
        return Social
            .findAll()
            .then( socials => res.status(200).send(socials))
    },
    retrieve(req, res){
        return Social
            .findById(req.params.socialId)
            .then( social => {
                if(!social) {
                    return res.status(400).send({
                        message: "страница не найдена"
                    })
                }
                return res.status(200).send(social)
            })
            .catch(err => res.status(400).send(err))
    },
    update(req, res) {
        return Social
            .findById(req.params.socialId)
            .then( social => {
                if(!social) {
                    return res.status(400).send({
                        message: "страница не найдена"
                    })
                }
                return social
                    .update({
                        link: req.body.link,
                        img: req.body.img,
                        title: req.body.title
                    })
                    .then(()=> res.status(200).send(social))
                    .catch( err => res.status(400).send(err));
            })
            .catch( err => res.status(400).send(err));
    },
    destroy(req, res) {
        return Social
            .findById(req.params.socialId)
            .then( social => {
                if(!social) {
                    return res.status(400).send({message: 'Страница не найдена' });
                }
                return social
                    .destroy()
                    .then( () => res.status(204).send())
                    .catch( err => res.status(400).send(err));
            })
            .catch( err => res.status(400).send(err));
    },

}