import React, { Component } from 'react';
import '../App.css';
import { Layout } from 'antd';
import CombinedCarousel from "./CombinedCarousel";
import Cards from './Cards';
import InfoGraphics from './InfoGraphics';
import GeographySection from './GeographySection';
import NewsBlock from './NewsBlock';
import SimpleCarousel from "./SimpleCarousel";
import Reports from "./Reports";
import moment from 'moment';

class App extends Component {

    state = {
        cards:{
            news:[],
            common: [],
        }
    };

    componentDidMount(){
        document.querySelector('title').innerHTML = this.props.locale === 'ru'? "Главная &mdash; ТАЛТЕК" : "Main &mdash TALTEK";
        if(window.location.pathname === '/'){
            document.querySelector('.visibilitynone').style.display = "flex";
        }

        fetch('/api/card')
            .then(res => res.json())
            .then( data => {

                const {cards} = this.state;
                const {news, common} = cards;

                data.map( card => {
                    if( card.is_news) news.push(card);
                    else common.push(card);
                    return void 0;
                });

                news.sort( (a,b) => {
                    return moment(b.news_date) - moment(a.news_date);
                });

                this.setState({cards: cards});

            })
    }

    render() {
        return (
            <Layout className="page-container visibilitynone" style={{ display: "none"}}>
                <CombinedCarousel locale={this.props.locale}/>
                <Cards locale={this.props.locale} pool={this.state.cards.common}/>
                <InfoGraphics locale={this.props.locale} />
                <GeographySection locale={this.props.locale} />
                <NewsBlock locale={this.props.locale} pool={this.state.cards.news}/>
                <SimpleCarousel locale={this.props.locale} slides={4}/>
                <Reports locale={this.props.locale}/>
            </Layout>
        );
    }
}

export default App;
