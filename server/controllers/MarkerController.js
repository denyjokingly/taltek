const Marker = require('../models').Markers;

module.exports = {
    create(req, res){
        return Marker
        .create({
            title_ru: req.body.title_ru,
            title_en: req.body.title_en,
            link: req.body.link,
            text_ru: req.body.text_ru,
            text_en: req.body.text_en,
            className: req.body.className
        })
        .then( marker => res.status(200).send(marker))
        .catch( err => res.status(400).send(err));
    },
    list(req, res){
        return Marker
        .findAll()
        .then( markers => res.status(200).send(markers))
    },
    retrieve(req, res){
        return Marker
        .findById(req.params.markerId)
        .then( marker => {
            if(!marker) {
                return res.status(404).send({
                    message: "страница не найдена"
                });
            }
            return res.status(200).send(marker);
        })
        .catch( err => res.status(400).send(err));
    },
    update(req, res){
        return Marker
        .findById(req.params.markerId)
        .then( marker => {
            if(!marker){
                return res.status(404).send({
                    message: 'todo not found'
                });
            }

            return marker
                .update( req.body, {
                    fields: Object.keys(req.body)
                })
                .then( marker=> res.status(200).send(marker))
                .catch( err => res.status(400).send(err));
        })
        .catch( err => res.status(400).send(err));
    },
    destroy(req, res){
        return Marker
        .findById(req.params.markerId)
        .then( marker => {
            if(!marker) {
                return res.status(400).send({message: 'Страница не найдена' });
            }
            return marker
                .destroy()
                .then( () => res.status(204).send())
                .catch( err => res.status(400).send(err));
        })
        .catch( err => res.status(400).send(err));
    },
}
