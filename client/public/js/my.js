//Hello
$(document).ready(function() {
  //Setup Load add Class
  $("#footer").addClass("closeMenu");
  $(".firstLevelMenu > a")
    .addClass("linkKiller")
    .addClass("close");
  $(".modal").addClass("close");
  $(".contentContactsMenuModal").addClass("hide");
  $("#barnaulContactsMenuModal")
    .removeClass("hide")
    .addClass("show");
  $(".backModal").addClass("hide");
  $(".linkMainMenu a").addClass("jumpToPage");
  $(".firstLevelMenu > a").removeClass("jumpToPage");
  $(".btSlideBigSlider a").addClass("jumpToPage");
  $("#mainMenu").hide();

  $("#logoTaltekHeader a").addClass("closeToPage");
  $("#buttonBackHome").addClass("closeToPage");

  //Open Modal Main Menu
  //Bar Icon Transformation
  $(".linkMenuBarHeader:nth-child(1)").show();
  $(".linkMenuBarHeader:nth-child(2)").hide();
  $(".linkMenuBarHeader:nth-child(3)").hide();

  $("#menuBarHeader").click(function() {
    if ($("#header").hasClass("closeMenu") == true) {
      $("#header")
        .removeClass("closeMenu")
        .addClass("openMenu");
      $("#footer")
        .removeClass("closeMenu")
        .addClass("openMenu");

      $("#mainMenu").show();

      $(".linkMenuBarHeader:nth-child(1)").hide();
      $(".linkMenuBarHeader:nth-child(2)").show();

      $("#menuModal")
        .removeClass("close")
        .addClass("open");
      $("#menu")
        .removeClass("close")
        .addClass("open");

      $(".contentSlideBigSlider")
        .removeClass("show")
        .addClass("hide");
      $("#controlSlideBigSlider")
        .removeClass("show")
        .addClass("hide");

      $(".backModal")
        .removeClass("hide")
        .addClass("show");
    } else if (
      $("#header").hasClass("openMenu") == true &&
      $("#header").hasClass("openMap") == false
    ) {
      $("#header")
        .removeClass("openMenu")
        .addClass("closeMenu");
      $("#footer")
        .removeClass("openMenu")
        .addClass("closeMenu");

      $(".linkMenuBarHeader:nth-child(1)").show();
      $(".linkMenuBarHeader:nth-child(2)").hide();
      $(".linkMenuBarHeader:nth-child(3)").hide();

      $("#menuModal")
        .removeClass("open")
        .addClass("close");
      $("#menu")
        .removeClass("open")
        .addClass("close");

      $(".contentSlideBigSlider")
        .removeClass("hide")
        .addClass("show");
      $("#controlSlideBigSlider")
        .removeClass("hide")
        .addClass("show");

      $(".backModal")
        .removeClass("show")
        .addClass("hide");

      $("ymaps").remove();

      $("#mainMenu")
        .delay(1000)
        .hide(200);
    } else if ($("#header").hasClass("openMap") == true) {
      $("#header").removeClass("openMap");
      $("#maps").removeClass("show");
      $("#menuModal").removeClass("openModalMap");
      $("#menu").removeClass("openModalMap");

      $("#mainMenu").show();

      $("#contactsMenuModal").css({ left: 0.5 * $(window).innerWidth() });

      $(".linkMenuBarHeader:nth-child(1)").hide();
      $(".linkMenuBarHeader:nth-child(2)").show();
      $(".linkMenuBarHeader:nth-child(3)").hide();

      $("#socialFooter").fadeIn(1000);
      $("#madeKodyFooter").fadeIn(1000);
      $("#langHeader").fadeIn(1000);
    }
  });

  //Open Modal Main Menu
  //Orientation Navigation Menu
  //Open Two Level Menu
  $(".twoLevelMenu").hide();
  $("#mainMenu").css({
    top: 0.5 * $(window).innerHeight() - 0.5 * $("#mainMenu").innerHeight() - 50
  });
  $("#mainMenu.forInternalPages").css({
    top: 0.5 * $(window).innerHeight() - 0.5 * $("#mainMenu").innerHeight() - 50
  });
  $(".backModal .filter").css({
    height: $(window).innerHeight() - 20,
    width: $(window).innerWidth() - 20
  });

  $("#mainMenu .linkKiller").on("click", function(e) {
    if ($(this).hasClass("close") == true) {
      $(".linkKiller")
        .removeClass("open")
        .addClass("close");
      $(this)
        .removeClass("close")
        .addClass("open");

      $(".close + .twoLevelMenu").hide(500);
      $(".open + .twoLevelMenu").show(500);
    } else if ($(this).hasClass("open") == true) {
      $(this)
        .removeClass("open")
        .addClass("close");

      $(".close + .twoLevelMenu")
        .delay(250)
        .hide(500);
    }

    e.preventDefault();
  });

  //Open Modal Main Menu
  //Orientation Contacts Block
  //Tab Info Contacts
  $("#contactsMenuModal").css({
    top:
      0.5 * $(window).innerHeight() - 0.5 * $("#mainMenu").innerHeight() - 120,
    left: 0.5 * $(window).innerWidth()
  });

  $(".contentContactsMenuModal.hide").fadeOut(500);

  $("#tabContacts .tab").on("click", function(e) {
    $("ymaps").remove();

    var dataTab = $(this).attr("data-tab-name");
    dataMaps = $(this).attr("data-maps-name");

    if ($(this).hasClass("active") == false) {
      $("#tabContacts .tab").removeClass("active");
      $(this).addClass("active");

      $(".contentContactsMenuModal")
        .removeClass("show")
        .addClass("hide");

      $("#" + dataTab + "ContactsMenuModal")
        .removeClass("hide")
        .addClass("show");

      $(".contentContactsMenuModal")
        .delay(250)
        .fadeOut(500);
      $("#" + dataTab + "ContactsMenuModal")
        .delay(750)
        .fadeIn(500);
    }

    e.preventDefault();
  });

  //Open Modal Main Menu
  //Open Maps
  //Tab Maps
  $(".linkToMap").on("click", function(e) {
    var dataMaps = $(this).attr("data-maps-name");

    $("#contactsMenuModal").css({ left: 50 });

    $("#header").addClass("openMap");
    $("#maps").addClass("show");
    $("#maps" + dataMaps).show();
    $("#menuModal").addClass("openModalMap");
    $("#menu").addClass("openModalMap");

    $("#mainMenu")
      .delay(1000)
      .hide(1000);

    $(".linkMenuBarHeader:nth-child(1)").hide();
    $(".linkMenuBarHeader:nth-child(2)").hide();
    $(".linkMenuBarHeader:nth-child(3)").show();

    $("#socialFooter").fadeOut(1000);
    $("#madeKodyFooter").fadeOut(1000);
    $("#langHeader").fadeOut(1000);
  });

  $(".langLink").each(function(i, node) {
    node.addEventListener("click", function() {
      document.cookie = `prefLang=${node.getAttribute("data-lang")}`;
    });
  });
});

$(window).on("load", function() {
  var suplink = document.querySelector(
    'a[href="' + location.pathname + '"].jumpToPage'
  );
  suplink && suplink.click();
});
