ymaps.ready(function() {
  $("[data-maps-name=Barnaul]").click(function() {
    // Barnaul
    var barnaulMap = new ymaps.Map("mapsBarnaul", {
      zoom: 15,
      center: [53.338086, 83.731024],
      controls: []
    });

    barnaulMap.controls.add("zoomControl", {
      size: "small",
      float: "none",
      position: {
        bottom: 60,
        right: 60
      }
    });

    var barnaulGeoObject = new ymaps.Placemark(
      [53.338086, 83.731024],
      {},
      {
        iconLayout: "default#image",
        iconImageHref: "/static/media/svg/geo.svg",
        iconImageSize: [32, 50],
        iconImageOffset: [-16, -50]
      }
    );

    barnaulMap.geoObjects.add(barnaulGeoObject);
  });

  $("[data-maps-name=Moscow]").click(function() {
    // Moscow
    var moscowMap = new ymaps.Map("mapsMoscow", {
      zoom: 15,
      center: [55.74183, 37.597405],
      controls: []
    });

    moscowMap.controls.add("zoomControl", {
      size: "small",
      float: "none",
      position: {
        bottom: 60,
        right: 60
      }
    });

    var moscowGeoObject = new ymaps.Placemark(
      [55.74183, 37.597405],
      {},
      {
        iconLayout: "default#image",
        iconImageHref: "/static/media/svg/geo.svg",
        iconImageSize: [32, 50],
        iconImageOffset: [-16, -50]
      }
    );

    moscowMap.geoObjects.add(moscowGeoObject);
  });
});
