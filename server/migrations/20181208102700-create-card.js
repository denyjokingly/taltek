'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
      return [
          queryInterface.addColumn('Cards', 'text_en', Sequelize.TEXT),
          queryInterface.addColumn('Cards', 'title_en', Sequelize.TEXT),
      ]
  }
};