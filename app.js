const express = require("express");
const logger = require("morgan");
const bodyParser = require("body-parser");
const path = require("path");
const fetch = require("node-fetch");
const busboy = require("connect-busboy");
const cookieParser = require("cookie-parser");

// Set up the express app
const app = express();

// Log requests to the console.
app.use(logger("dev"));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(busboy());
app.use("/static", express.static(__dirname + "/client/build/static"));
app.use(
  "/static/upload/docs",
  express.static(__dirname + "/static/upload/docs")
);
app.use(
  "/static/upload/assets",
  express.static(__dirname + "/static/upload/assets")
);

require("./server/routes")(app);
app.set("view engine", "ejs");

app.get("/admin/*", function(req, res) {
  res.sendFile(__dirname + "/client/build/index.html");
});

app.get("/admin", function(req, res) {
  res.sendFile(__dirname + "/client/build/index.html");
});

app.get("*", async function(req, res) {
  const { url } = req;

  const base = req.headers.host;
  const proto = req.protocol;
  const slider = await fetch(`${proto}://${base}/api/slider`)
    .then(res => res.json())
    .then(sliders => sliders[0])
    .catch(err => console.log(err));

  const navigation = await fetch(`${proto}://${base}/api/page`)
    .then(res => res.json())
    .then(pages => {
      const nav = [];
      pages.map(page => {
        if (page.is_main) nav.push({ parent: page, children: [] });
      });
      const bottom = pages.filter(page => !page.is_main);

      nav.map(page => {
        bottom.map(bottomPage => {
          if (page.parent.id === bottomPage.childPageId) {
            page.children.push(bottomPage);
          }
        });
      });
      nav.sort((a, b) => {
        return a.parent.idx - b.parent.idx;
      });
      nav.map(node => {
        node.children.sort((a, b) => {
          return a.idx - b.idx;
        });
      });

      return nav;
    });

  const social = await fetch(`${proto}://${base}/api/social`).then(res =>
    res.json()
  );

  let lang = req.cookies.prefLang;
  if (!lang) {
    lang = "ru";
  }

  res.cookie("prefLang", lang, { maxAge: 865e5 }).render("home", {
    slider: slider,
    navigation: navigation,
    social: social,
    lang: lang
  });
});

module.exports = app;
