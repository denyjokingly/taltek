import React, {Component} from 'react';
import { List, Popconfirm } from 'antd';
import { Link } from 'react-router-dom';
import {  Button, Icon, Divider } from 'antd';

export default class Pages extends Component {
    constructor(){
        super();
        this.state = {
            pages: [],
        }
    }

    componentDidMount() {
        this.getPages();
    }

    getPages() {
        fetch('/api/page')
        .then( res=>  res.json() )
        .then( pages => this.setState({pages: pages}))
        .catch(err => console.log(err));
    }

    deletePage(itemId){
        fetch(`/api/page/${itemId}`,{
            method: "DELETE"
        })
        .then( () => this.getPages())
        .catch( err => console.log(err) );
    }

    render(){
        const {pages} = this.state;
        debugger;
        return (
            <div className="pages-list-wrapper">
                <h2>Список страниц:</h2>
                <Divider />
                <List 
                itemLayout="horizontal"
                dataSource={pages}
                size="small"
                renderItem= { item => (
                    <List.Item actions={[<Link to={`/admin/edit/page/${item.id}`}><Icon type="edit"/></Link>,
                        <Popconfirm title="Подтвердите действие" 
                                    okText="Да" 
                                    cancelText="Нет" 
                                    onCancel={ () => console.log('cancel')} 
                                    onConfirm={this.deletePage.bind(this,`${item.id}`)}>
                                        <a><Icon type="close"/></a>
                                    </Popconfirm> ]}>
                        {item.title_ru}
                        <Popconfirm>sadsa</Popconfirm>
                    </List.Item>
                )}
                />
                <Link to="/admin/create/page" className="pages-list__add-button">
                    <Button type="dashed" style={{ width: '60%' }}>
                        <Icon type="plus" /> Добавить страницу
                    </Button>
                </Link>
            </div>

        )
    }
}