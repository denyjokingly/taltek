import React, { Component } from 'react';
import { Row, Col } from 'antd';
import { Radio } from 'antd';
import {Link} from 'react-router-dom';
import arrow from '../images/arrows-link.svg';
import moment from 'moment';

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

export default class NewsBlock extends Component {

    state = {
        filter:"pr"
    };

    filter = (card) =>{
        return '';
    };

    onChange = e => {
        this.setState({filter: e.target.value});
    };

    render() {
        return (
            <div style={{ display: this.props.locale == 'ru' ? 'flex' : 'none'}} className="main-page-content">
                <Row type="flex" justify="around" align="bottom" style={{width:"100%"}}>
                    <Col className="news-heading-container" style={{margin: ""}}>
                        <h1 style={{margin:0}} className="news-heading">Медиа-центр</h1>
                    </Col>
                    <Col className="flex-center-fluid tags">
                        <RadioGroup onChange={this.onChange} defaultValue={this.state.filter}>
                            <RadioButton value="pr">Пресс-релизы</RadioButton>
                            <RadioButton value="news">Новости</RadioButton>
                        </RadioGroup>
                    </Col>
                    <Col className="flex-center-fluid news-open" style={{marginBottom: 5}}>
                        <Link to={`/media/${this.state.filter}`} className="news__main-link">
                            {this.props.locale === 'ru' ? "Перейти к разделу" : "Go to section"}
                            &nbsp;<img src={arrow} style={{color: "inherit"}}/>
                        </Link>
                    </Col>
                </Row>
                <Row >
                    <div className="newsblock-normal">
                    {this.props.pool && this.props.pool.filter( i => i.tags === this.state.filter )
                        .map( (x,i) => {
                        const {filter} = this.state;
                        x.img = x.img.split('').map( char => {
                            return char == '\\' ? "/" : char;
                        }).join('');
                        if(i > 5) return;
                        return (
                            <Col key={i} className={ x.img === "" ? "news-item--no-image" : "news-item--image"}
                                 style={{ backgroundImage: "url("+x.img+")", backgroundSize: "cover",
                                        display: x.tags === this.state.filter ? "flex" : "none"}} span={8}>
                                <span className="news-item__date">{moment(x.news_date).format('LL')}</span>
                                <h1 className="news-item__heading">{x['title_'+this.props.locale]}</h1>
                                <Link to={`/media/${x.tags}/${x.id}`} dangerouslySetInnerHTML={{__html: this.props.locale === 'ru' ? "Подробнее    &xrarr;":"Learn more    &xrarr;"}}/>
                            </Col>
                        )
                    })}
                    </div>
                    <div className="newsblock-mobile">
                        {this.props.pool && this.props.pool.filter( i => i.tags === this.state.filter).map( (x,i) => {
                            const {filter} = this.state;
                            x.img = x.img.split('').map( char => {
                                return char == '\\' ? "/" : char;
                            }).join('');
                            if(i > 5) return null;
                            return (
                                <Col key={i} className="news-item-mobile">
                                    <span className="news-item__date-mobile">{moment(x.news_date).format('LL')}</span>
                                    <Link to={`/media/${x.tags}/${x.id}`}>
                                        <h1 className="news-item__heading-mobile">{x['title_'+this.props.locale]}</h1>
                                    </Link>
                                </Col>
                            )
                        })}
                    </div>
                </Row>
            </div>
        );
    }
}

