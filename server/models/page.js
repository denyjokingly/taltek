'use strict';
module.exports = (sequelize, DataTypes) => {
  var Page = sequelize.define('Page', {
    title_en: DataTypes.STRING,
    title_ru: DataTypes.STRING,
    nav_name_en: DataTypes.STRING,
    nav_name_ru: DataTypes.STRING,
    url: DataTypes.STRING,
    text_ru: DataTypes.TEXT,
    text_en: DataTypes.TEXT,
    is_main: DataTypes.BOOLEAN,
    visible: DataTypes.BOOLEAN,
    childPageId: DataTypes.INTEGER,
    idx: DataTypes.INTEGER,
    script: DataTypes.TEXT,
  }, {});
  Page.associate = function(models) {
    // associations can be defined here
  };
  return Page;
};