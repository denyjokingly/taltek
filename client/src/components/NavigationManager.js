import React, {Component} from 'react';
import {Table, Checkbox, Select, Button, Icon, Input, Popconfirm} from 'antd';
import {Link} from 'react-router-dom';
import {message} from "antd/lib/index";
import './NavigationManager.css';

const Option = Select.Option;
const success = () => {
    message.success('Изменения успешно применены');
};
const error = (err) => {
    message.error('Произошла ошибка \n',err);
};

export default class NavigationManager extends Component {
    constructor(){
        super();
        this.state = {
            sitemap: [],
            pages: [],
            taskqueue: []
        };

        this.columns = [
            {
              title: "id",
              dataIndex: 'id',
              key: 'id',
              sorter: (a,b) => {
                  return a.id - b.id;
              },
              sortOrder: 'ascend',
            },
            {
                title: 'Название',
                dataIndex: 'nav_name_ru',
                key: 'nav_name_ru'
            },
            {
                title: 'Ссылка',
                dataIndex: 'url',
                key: 'url'
            },
            {
                title: 'Очередность',
                dataIndex: 'idx',
                key: 'idx',
                render: (text, record) => (
                    <Input name={`${record.id}`} value={record.idx} onChange={this.updateIdx.bind(this)} style={{width: 70}} defaultValue={record.idx}/>
                )
            },
            {
                title: 'Корневая',
                key: 'rootPage',
                render: (text, record) => (
                    <Checkbox onChange={this.handleChange.bind(this, record)} defaultChecked={record.is_main}/>
                )
            },
            {
                title: 'Скрыть',
                key: 'visible',
                render: (text, record) => (
                    <Checkbox onChange={this.handleVisible.bind(this, record)} defaultChecked={record.visible}/>
                )
            },
            {
                title: 'Дочерняя страница',
                key: 'childPage',
                sorter: false,
                render: (text, record) => (
                    <Select disabled={record.is_main}
                        onChange={this.selectMain.bind(this, record)}
                        style={{ width: "200px"}}
                        defaultValue={this.getDefaultPage(record)}
                    >
                        <Option key="null" value={null}>----</Option>
                        {
                            this.state.pages.filter( page => page.is_main ).map( page => this.getOption(page))
                        }
                    </Select>
                )
            },
            {
                title: 'Действия',
                key: 'actions',
                sorter: false,
                render: (text, record) => (
                    <div>
                        <Link className="link-override" to={`/admin/edit/page/${record.id}`} style={{marginRight: 10}}><Icon type="edit"/></Link>
                        <Popconfirm title="Подтвердите действие" 
                                    okText="Да" 
                                    cancelText="Нет" 
                                    onCancel={ () => console.log('cancel')} 
                                    onConfirm={this.deletePage.bind(this,`${record.id}`)}><a className="link-override" href="javascript:;"><Icon type="close"/></a></Popconfirm>
                    </div>
                )
            }
        ];
    }

    componentDidMount() {
        fetch('/api/page')
            .then( res=>  res.json() )
            .then( pages => this.setState({pages: pages}))
            .catch(err => error(err));
    }

    getDefaultPage(record){
        const [page] = this.state.pages.filter( page => page.id === record.childPageId );
        if(!page) return "----";
        return `${page.nav_name_ru}`;
    }

    updateIdx(e){
        const {value, name} = e.target;
        const {pages} = this.state;
        pages.map( page => {
            if(page.id == name){
                page.idx = value;
                this.state.taskqueue.push(page);
            }
        });

        this.setState({pages: pages});
    }

    handleVisible(record){
        fetch('/api/page/visibility/'+record.id, {
            method: "POST",
            headers: {
                "Content-type":"application/json"
            },
            body: JSON.stringify({visible: !record.visible})
        })
        .then( res => res.json())
        .then( () => this.getPages())
        .catch( err => console.log(err));
    }

    getPages() {
        fetch('/api/page')
            .then( res=>  res.json() )
            .then( pages => this.setState({pages: pages}))
            .then( () => success())
            .catch(err => error(err));
    }

    deletePage(itemId){
        fetch(`/api/page/${itemId}`,{
            method: "DELETE"
        })
            .then( () => this.getPages())
            .catch( err => console.log(err) );
    }

    getOption(page){
        return (
            <Option key={page.id} value={page.id} >{page.nav_name_ru}</Option>
        )
    }

    setMain(pageId, isMain) {
        console.log(isMain);
        fetch('/api/page/navigation/'+pageId,{
            method:"POST",
            headers: {
                "Content-type":"application/json"
            },
            body: JSON.stringify({is_main: isMain})
        })
            .then( res => res.json() )
            .then( () => this.getPages())
            .catch(err => console.log(err));
    }

    handleChange(record) {
        this.setMain(record.id, !record.is_main);
    }

    selectMain(value, record){
        value.childPageId = record;
        this.updatePage(value);
    }

    saveChanges() {
        this.state.taskqueue.map( task => {
            this.updatePage(task);
        });
        this.setState({taskqueue:[]});
    }

    updatePage(page){
        fetch(`/api/page/${page.id}`,{
            method: "POST",
            headers: {
                "Content-type":"application/json"
            },
            body: JSON.stringify(page)
        })
            .then( res => res.json() )
            .then( () => this.getPages())
            .catch(err => console.log(err));
    }

    render(){
        return (
            <div className="page-container">
                <div style={{display: "flex", justifyContent:"space-between"}}>
                <Link to="/admin/create/page" className="pages-list__add-button" style={{marginLeft: "auto", marginRight:"auto"}}>
                    <Button type="ghost" style={{ width: 250, marginBottom: 20 }}>
                        <Icon type="plus" /> Добавить страницу
                    </Button>
                </Link>
                <Button type="primary"  onClick={this.saveChanges.bind(this)}>
                     Сохранить
                </Button>
                </div>
                <Table rowKey={ record => record.id } columns={this.columns} dataSource={this.state.pages}/>
            </div>
        )
    }
}