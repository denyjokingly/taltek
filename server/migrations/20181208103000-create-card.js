'use strict'
module.exports = {
    up: (queryInterface, Sequelize) => {
        return [
            queryInterface.renameColumn('Cards','text', 'text_ru'),
            queryInterface.renameColumn('Cards','title', 'title_ru'),
        ]
    }
};