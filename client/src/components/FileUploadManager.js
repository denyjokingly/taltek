import React, {Component} from 'react';
import { List, Upload, message, Button, Icon } from 'antd';
import {Link} from 'react-router-dom';
const props = {
    name: 'uploadFile',
    action: '/api/upload/doc',
    onChange(info) {
        if (info.file.status !== 'uploading') {
            // console.log(info.file, info.fileList);
        }
        if (info.file.status === 'done') {
            message.success(`Файл ${info.file.name} успешно загружен`);
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} не смог быть загружен.`);
        }
    },
}

export default class FileUploadManager extends Component {

    state = {
        docs: []
    };

    getDocs(){
        fetch('/api/upload/doc')
            .then( res => res.json())
            .then( docs => this.setState({docs: docs}))
            .catch( err => console.log(err));
    }

    componentDidMount(){
        this.getDocs();
    }

    removeFile = file => {
      fetch('/api/upload/doc/'+file.name, {
          method: 'DELETE',
      })
          .then( () => this.getDocs() )
          .catch( err => console.log(err));
    };

    render() {
        return (
            <div className="page-container">
                <h1>Документы :</h1>
                <Upload {...props} showUploadList={false} onChange={this.getDocs.bind(this)} accept='application/pdf'>
                    <Button style={{marginTop: 10, marginBottom: 10}}>
                        <Icon type="upload" /> Загрузить файл
                    </Button>
                </Upload>
                <List
                    itemLayout="horizontal"
                    dataSource={this.state.docs}
                    pagination={{
                        onChange: (page) => {
                            // console.log(page);
                        },
                        pageSize: 30,
                    }}
                    renderItem={ item => (
                        <List.Item
                            actions={[<a onClick={this.removeFile.bind(this, item)}><Icon type="delete"/></a>]}>
                            <List.Item.Meta
                                title={<Link to={item.path}>{item.name}</Link>}
                                description={"Создан : "+new Date(item.created).toLocaleDateString()+" / Размер : "+item.size}/>
                        </List.Item>
                    )}/>
            </div>
        )
    }
}