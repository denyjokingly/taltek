const pageCtrl = require('./PageController');
const sliderCtrl = require('./SliderController');
const cardsCtrl = require('./CardController');
const cssController = require('./CSSController');
const socialController = require('./SocialController');
const fileUploadController = require('./FileUploaderController');
const markerController = require('./MarkerController');
const authController = require('./authController');

module.exports = {
    authController,
    pageCtrl,
    sliderCtrl,
    cardsCtrl,
    cssController,
    socialController,
    fileUploadController,
    markerController,
};