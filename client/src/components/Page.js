import React, {Component} from 'react';
import { Form, Input, Button, message, Row, Col, Tabs } from 'antd';
import Template from './Template';

const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const success = () => {
    message.success('Изменения успешно применены');
};
const error = () => {
    message.error('Произошла ошибка');
};
const CustomizedForm = Form.create({
    onFieldsChange(props, changedFields){
        props.onChange(changedFields);
    },
    mapPropsToFields(props) {
        return {
            title_ru: Form.createFormField({
                ...props.title_ru,
                value: props.title_ru.value,
            }),
            title_en: Form.createFormField({
                ...props.title_en,
                value: props.title_en.value
            }),
            nav_name_en: Form.createFormField({
                ...props.nav_name_en,
                value: props.nav_name_en.value
            }),
            nav_name_ru: Form.createFormField({
                ...props.nav_name_ru,
                value: props.nav_name_ru.value
            }),
            url: Form.createFormField({
                ...props.url,
                value: props.url.value
            }),
            text_ru: Form.createFormField({
                ...props.text_ru,
                value: props.text_ru.value
            }),
            text_en: Form.createFormField({
                ...props.text_en,
                value: props.text_en.value
            }),
            script: Form.createFormField({
                ...props.script,
                value: props.script.value
            })
        };
    },
    onValuesChange(_, values) {
        //console.log(values);
    },
})((props) => {
    const { getFieldDecorator } = props.form;
    return (
        <Row>
            <Col span={12} offset={1}>
                <Form onSubmit={props.onSubmit}>
                    <FormItem
                        label="Заголовок (английский)">
                        {getFieldDecorator('title_en',{
                            rules: [{required: true, message: "Поле не должно быть пустым"}]
                        })(<Input />)}
                    </FormItem>
                    <FormItem
                        label="Заголовок (русский)">
                        {getFieldDecorator('title_ru',{
                            rules: [{required: true, message: "Поле не должно быть пустым"}]
                        })(<Input />)}
                    </FormItem>
                    <FormItem
                        label="Отображать в навигации как (английский)">
                        {getFieldDecorator('nav_name_en',{
                            rules: [{required: true, message: "Поле не должно быть пустым"}]
                        })(<Input />)}
                    </FormItem>
                    <FormItem
                        label="Отображать в навигации как (русский)">
                        {getFieldDecorator('nav_name_ru',{
                            rules: [{required: true, message: "Поле не должно быть пустым"}]
                        })(<Input />)}
                    </FormItem>
                    <FormItem
                        label="Cсылка на страницу (относительный путь)">
                        {getFieldDecorator('url',{
                            rules: [{required: true, message: "Поле не должно быть пустым"}]
                        })(<Input />)}
                    </FormItem>

                    <Button type="primary" htmlType="submit"
                            disabled={
                                props.title_ru === '' ||
                                props.nav_name_ru === '' ||
                                props.url === ''
                            }
                    >Сохранить</Button>
                    {!props.isNew && <Button type="danger" onClick={props.deletePage} style={{marginLeft: "5px"}}>Удалить</Button>}
                </Form>
            </Col>
        </Row>
    );
});

message.config({
    maxCount: 1,
    duration:2,
});

export default class Page extends Component {
    constructor() {
        super();
        this.state = {
            isNew : true,
            fields: {
                title_ru: {
                    value: ''
                },
                title_en: {
                    value: ''
                },
                url: {
                    value: ''
                },
                nav_name_en: {
                    value: ''
                },
                nav_name_ru: {
                    value: ''
                },
                text_ru: {
                    value: ''
                },
                text_en: {
                    value: ''
                },
                script: {
                    value: ''
                }
            },
            content: []
        };
    }

    componentDidMount() {
        this.getPage();
    }

    getPage() {
        const { match: { params } } = this.props;
        if( !params.pageId ) return;
        fetch(`/api/page/${params.pageId}`)
            .then( res => res.json())
            .then( page => this.patchFields(page));
    }

    deletePage(){
        const { match: { params } } = this.props;
        if( !params.pageId ) return;
        fetch(`/api/page/${params.pageId}`,{
            method: 'DELETE',
        })
        .then( () => this.props.history.push('/'))
        .catch( err => console.log(err));

    }

    handleFormChange = (changedFields) => {
        this.setState(({ fields }) => ({
            fields: { ...fields, ...changedFields },
        }));
    };

    patchFields(page) {
        const updatedFields = Object.assign({}, this.state.fields);
        for(let prop in page){
            if(updatedFields[prop]) updatedFields[prop].value = page[prop];
        }
        this.setState((state) => ({isNew: false, fields: updatedFields }));
        console.log(this.state.fields);
    }

    
    sendUpdate = (e) => {
        e.persist();
        e.preventDefault();

        let pageToUpdate = {
            title_ru: this.state.fields.title_ru.value,
            title_en: this.state.fields.title_en.value,
            nav_name_en: this.state.fields.nav_name_en.value,
            nav_name_ru: this.state.fields.nav_name_ru.value,
            url: this.state.fields.url.value,
            text_ru: this.state.fields.text_ru.value,
            text_en: this.state.fields.text_en.value,
            script: this.state.fields.script.value
        };
        const { match: { params } } = this.props;
        const route = this.state.isNew ? '' : `${params.pageId}`;

        fetch(`/api/page/${route}`, {
            method: 'POST',
            cache: 'no-cache',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify(pageToUpdate)
        })
        .then( res => res.json() )
        .then( page => {
            success();
            this.patchFields(page)
        })
        .catch( err=> {
            error();
            console.log(err);
            this.getPage();
        })
    };

    render(){
        const fields = this.state.fields;

        return (
            <div className="page-container">
                <Tabs defaultActiveKey="1">
                    <TabPane tab="Основные параметры" key="1">
                        <CustomizedForm {...fields} onChange={this.handleFormChange} deletePage={this.deletePage.bind(this)} isNew={this.state.isNew} onSubmit={this.sendUpdate}/>
                    </TabPane>
                    <TabPane tab="Контент" key="2">
                        <Template page={this.props.match.params}/>
                    </TabPane>
                </Tabs>
            </div>
        )
    }
}