const Slider = require('../models').Slider;

module.exports = {
    create(req, res){
      return Slider
          .create({
              content: req.body.content
          })
          .then( slider => res.status(200).send(slider))
          .catch( err => res.status(400).send(err));
    },
    list(req, res) {
      return Slider
           .findAll()
           .then( sliders => res.status(200).send(sliders))
    },
    retrieve(req, res) {
        return Slider
            .findById(req.params.sliderId)
            .then( slider => {
                if(!slider) {
                    return res.status(404).send({
                        message: "страница не найдена"
                    });
                }
                return res.status(200).send(slider);
            })
            .catch( err => res.status(400).send(err));
    },
    update(req, res) {
        return Slider
            .findById(req.params.sliderId)
            .then( slider => {
                if(!slider){
                    console.log('Page not found!');
                    return res.status(404).send({
                        message: 'slider not found'
                    });
                }

                return slider
                    .update({
                        content: req.body || slider.content
                    })
                    .then(()=> res.status(200).send(slider))
                    .catch( err => res.status(400).send(err));
            })
            .catch( err => res.status(400).send(err));
    },
    destroy(req, res) {
        return Slider
            .findById(req.params.sliderId)
            .then( slider => {
                if(!slider) {
                    return res.status(400).send({message: 'Страница не найдена' });
                }
                return slider
                    .destroy()
                    .then( () => res.status(204).send())
                    .catch( err => res.status(400).send(err));
            })
            .catch( err => res.status(400).send(err));
    },
};