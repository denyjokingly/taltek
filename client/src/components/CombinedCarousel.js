import React, { Component } from 'react';
import {Carousel} from 'antd';
import {Link} from 'react-router-dom';
import arrow from '../images/arrows-link.svg';

function SamplePrevArrow(props) {
    const { klass, style, onClick } = props;
    return (
      <div
        className={klass}
        style={{ ...style, display: "block",
        backgroundImage: "url(/static/upload/assets/arrows.svg)",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        content: "none",
        height: 40,
        width: 40,
        position:"absolute",
        cursor: 'pointer',
        zIndex: 99}}
        onClick={onClick}
      />
    );
  }

const options = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: <SamplePrevArrow klass='prevArrow'/>,
    nextArrow: <SamplePrevArrow klass='nextArrow'/>,
    appendDots: dots => (
        <div>
            <ul className="carousel-wrapper__controls"> {dots} </ul>
        </div>
    ),
    customPaging: i => (
        <div className="carousel-wrapper__controls__dot">
        </div>
    )
};

export default class CombinedCarousel extends Component{

    constructor(){
        super();
        this.onWindowResized = this.onWindowResized.bind(this);

    }
    state = {
      slider:{},
      currentSlide: 0
    };

    componentWillUnmount () {
        window.removeEventListener('resize', this.onWindowResized);
    }

    onWindowResized() { //add debounce for performance?
        this.forceUpdate(); //react's default forceUpdate
    }

    componentDidMount(){
        fetch('/api/slider')
            .then(res=>res.json())
            .then(sliders => {
                const [slider] = sliders.filter( slider => slider.content.id == 'top-slider');
                this.setState({slider: slider})
            })
            .catch(err => console.log(err));
        window.addEventListener('resize', this.onWindowResized);

    }

    updateSlide(current){
        this.setState( ()=> ({currentSlide: current}));
    }

    render(){
        const {locale} = this.props;

        return (
            <div id={this.state.slider.content && this.state.slider.content.id} className="carousel-wrapper">
                <Carousel {...options} afterChange={this.updateSlide.bind(this)}>
                    {this.state.slider.content && this.state.slider.content.slides.map( (x,i) =>{
                        return (<div key={i}>
                            <div className="carousel-wrapper__image"
                                 style={{backgroundImage: "url("+x.img.split('').map(
                                     c => c === '\\'? "/": c).join('')+")", backgroundPosition: "cover"}}/>
                            </div>)
                    })}
                </Carousel>
                <div className="carousel-wrapper__description">
                    { this.state.slider.content && this.state.slider.content.slides.map( (slide,idx) => {
                        return (
                            <div style={{display: this.state.currentSlide === idx ? "flex":"none"}} key={idx}
                                 className="carousel-wrapper__description__group">
                                <h1 dangerouslySetInnerHTML={{__html: slide['text_'+this.props.locale]}}/>
                                <Link to={slide.url}
                                      dangerouslySetInnerHTML={{__html:
                                              this.props.locale === 'ru' ? "Подробнее    &xrarr;":"Learn more    &xrarr;"}}>
                                </Link>
                            </div>
                        )
                    })}

                </div>
            </div>
        )
    }
}