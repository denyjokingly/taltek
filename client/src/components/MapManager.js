import React, {Component} from 'react';
import { List, Popconfirm } from 'antd';
import { Link } from 'react-router-dom';
import {  Button, Icon, Divider } from 'antd';

export default class Pages extends Component {
    constructor(){
        super();
        this.state = {
            markers: [],
        }
    }

    componentDidMount() {
        this.getMarkers();
    }

    getMarkers() {
        fetch('/api/marker')
        .then( res=>  res.json() )
        .then( markers => this.setState({markers}))
        .catch(err => console.log(err));
    }

    deleteMarker(itemId){
        fetch(`/api/marker/${itemId}`,{
            method: "DELETE"
        })
        .then( () => this.getMarkers())
        .catch( err => console.log(err) );
    }

    render(){
        const {markers} = this.state;
        return (
            <div className="pages-list-wrapper">
                <h2>Список геометок:</h2>
                <Divider />
                <List 
                itemLayout="horizontal"
                dataSource={markers}
                size="small"
                renderItem= { item => (
                    <List.Item actions={[<Link to={`/admin/edit/marker/${item.id}`}><Icon type="edit"/></Link>,
                        <Popconfirm title="Подтвердите действие" 
                        okText="Да" 
                        cancelText="Нет" 
                        onCancel={ () => console.log('cancel')} 
                        onConfirm={ this.deleteMarker.bind(this,`${item.id}`) }><a><Icon type="close"/></a></Popconfirm> ]}>
                        {item.title_ru}
                    </List.Item>
                )}
                />
                <Link to="/admin/create/marker" className="pages-list__add-button">
                    <Button type="dashed" style={{ width: '60%' }}>
                        <Icon type="plus" /> Добавить геометку
                    </Button>
                </Link>
            </div>

        )
    }
}