'use strict';
module.exports = (sequelize, DataTypes) => {
  var Slider = sequelize.define('Slider', {
    content: DataTypes.JSON
  }, {});
  Slider.associate = function(models) {
    // associations can be defined here
  };
  return Slider;
};