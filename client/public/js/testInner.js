$(document).ready(function() {
  var urlName = $(this).attr("href");
  urlBack = "url(/static/media/jpg/inner-back.jpg)";

  if ($("body").hasClass("testInnerPage") == true) {
    $("#menu")
      .removeClass("close")
      .addClass("open");
    $("#mainMenu").show();

    $("#contentPage").css("-webkit-transform", "translateY(0)");
    $("#contentPage").css("opacity", "1");
    $("#backPage").css({ "z-index": 1 });

    $("#logoTaltekHeader img").addClass("closeToPage");
    $("#buttonBackHome").addClass("closeToPage");

    $("#backPage").css({
      opacity: 1,
      "background-image": urlBack
    });

    $("#backPage").css({});

    $("#contentPage").html($("#contentPage").innerWidth());
  }
});
