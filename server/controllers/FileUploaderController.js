const request = require('request');
const fs = require('fs');
const crypto = require('crypto');
const path = require('path');
const formidable = require('formidable');
const DOC_PATH = './static/upload/docs/';
const ASSETS_PATH = './static/upload/assets/';

const normalize = size => {
    let fmts;

    if(size < 1000 ){
        fmts = size+' B';
    } else if( size < 1000000) {
        fmts = Math.ceil(size/1000) + ' Kb'
    } else if( size < 1000000000) {
        fmts = Math.ceil(size/1000000) + 'Mb'
    }

    return fmts;
};

module.exports = {
  uploadDoc(req, res){
    const form = new formidable.IncomingForm();
      form.parse(req, (err, fields, files) => {
        console.log(files.uploadFile.name);
        fs.rename(files.uploadFile.path, DOC_PATH+files.uploadFile.name, (err)=>{
          if(err){
            res.status(500).send(err);
          } else {
              res.status(200).send('ok');
          }
        })
      })
  },
  uploadAsset(req, res){
    const form = new formidable.IncomingForm();
      form.parse(req, (err, fields, files) => {
        console.log(files.uploadFile.name);
        fs.rename(files.uploadFile.path, ASSETS_PATH+files.uploadFile.name, (err)=>{
          if(err){
            res.status(500).send(err);
          } else {
              res.status(200).send('ok');
          }
        })
      })
  },
  retrieveDocs(req, res){
    const content = fs.readdirSync(DOC_PATH);
    let files = [];
    content.map( file => {
      const item = fs.createReadStream(DOC_PATH+file);
      const {size, birthtime} = fs.statSync(DOC_PATH+file);
      files.push({
          name: item.path.split('/')[4],
          path: item.path,
          size: normalize(size),
          created: birthtime,
          file: item
      });
    });

    files.sort(function(a,b){
        return b.created - a.created
    });
    res.send(files);
  },
  retrieveAssets(req, res){
    const content = fs.readdirSync(ASSETS_PATH);
    let files = [];
    content.map( file => {
      const item = fs.createReadStream(ASSETS_PATH+file);
      const {size, birthtime} = fs.statSync(ASSETS_PATH+file);
      files.push({
          name: item.path.split('/')[4],
          path: item.path,
          size: normalize(size),
          created: birthtime,
          file: item
      });
    });

    files.sort(function(a,b){
        return b.created - a.created
    });
    res.send(files);
  },
  deleteDoc(req, res){
    const {file} = req.params;

    try{
        fs.unlinkSync(path.resolve(DOC_PATH, file));
        res.status(200).send('file removed');
    } catch (e) {
        res.status(400).send(e);
    }
  },
  deleteAsset(req, res){
    const {file} = req.params;

    try{
        fs.unlinkSync(path.resolve(ASSETS_PATH, file));
        res.status(200).send('file removed');
    } catch (e) {
        res.status(400).send(e);
    }
  },
  downloadFile(req, res) {
      const {file} = req.params;

      const rs = fs.createReadStream(path.join(DOC_PATH, file));
      rs.pipe(res);
  },
  downloadAsset(req, res){
      const {link} = req.body;
      const ext = link.match(/jpg|png|webp/);
      const id = crypto.randomBytes(16).toString("hex");
      const dest = path.posix.join(ASSETS_PATH, `asset__${id}.${ext}`).split('').map( char => {
          return char == '\\' ? "/" : char;
      }).join('');

      if(ext === null){
          res.status(400).text('Bad format');
      }

      const file = fs.createWriteStream(dest);
      const sendReq = request.get(link);

      const cb = message => {
          res.send(message);
      }

      // verify response code
      sendReq.on('response', function(response) {
          if (response.statusCode !== 200) {
              return cb('Response status was ' + response.statusCode);
          }
      });

      // check for request errors
      sendReq.on('error', function (err) {
          fs.unlink(dest);
          return cb(err.message);
      });

      sendReq.pipe(file);

      file.on('finish', function() {
          file.close();
          res.status(200).send(JSON.stringify({file:dest}));
      });

      file.on('error', function(err) { // Handle errors
          fs.unlink(dest); // Delete the file async. (But we don't check the result)
          return cb(err.message);
      });
  }

};