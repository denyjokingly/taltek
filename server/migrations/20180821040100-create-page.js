'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return [
            queryInterface.addColumn('Pages', 'visible', Sequelize.BOOLEAN),
        ]
    },
};