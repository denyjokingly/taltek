import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import Page from "./Page";
import { Layout, Menu, Icon, Affix, message } from "antd";
import NavigationManager from "./NavigationManager";
import SliderManager from "./SliderManager";
import CardsManager from "./CardsManager";
import NewsManager from "./NewsManager";
import Card from "./Card";
import CSS from "./CSS";
import News from "./News";
import Slider from "./Slider";
import Settings from "./Settings";
import Social from "./Social";
import FileUploadManager from "./FileUploadManager";
import StorageUploadManager from "./StorageUploadManager";
import MapManager from "./MapManager";
import Marker from "./Marker";
import logo from "../images/logotype-white.svg";
import Login from "./Login";
const { Header, Content, Sider } = Layout;

const error = text => {
  message.error(text);
};

export default class Dashboard extends Component {
  state = {
    collapsed: false,
    isAuth: false,
    user: {
      role: ""
    }
  };

  componentDidMount() {}

  authenticate = values => {
    fetch("/api/user/authenticate", {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify({
        login: values.userName,
        password: values.password
      })
    })
      .then(res => res.json())
      .then(res => {
        console.log(res);
        if (res.message) {
          error(res.message);
          return;
        }
        this.setState({ isAuth: !!res, user: res[0] });
      });
  };

  onCollapse = collapsed => {
    this.setState({ collapsed });
  };

  render() {
    console.log(this.state.user);
    return !this.state.isAuth ? (
      <div className="page-container">
        <div className="page-section">
          <div className="page-content">
            <Login locale={this.props.locale} onChange={this.authenticate} />
          </div>
        </div>
      </div>
    ) : (
      <Layout style={{ minHeight: "100vh" }}>
        <Sider
          collapsible
          collapsed={this.state.collapsed}
          onCollapse={this.onCollapse}
        >
          <Affix offsetTop={0} className="overflow-hidden">
            <div className="logo">
              <img
                src={logo}
                style={{ maxWidth: "60%", margin: 24, display: "block" }}
              />
            </div>
            {this.state.user.role !== "admin" ? (
              <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
                <Menu.Item key="1">
                  <Link className="link-override" to="/admin/news">
                    <Icon type="bars" />
                    <span>Новости</span>
                  </Link>
                </Menu.Item>
                <Menu.Item key="8">
                  <Link className="link-override" to="/admin/storage">
                    <Icon type="cloud" />
                    <span>Хранилище</span>
                  </Link>
                </Menu.Item>
              </Menu>
            ) : (
              <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
                <Menu.Item key="1">
                  <Link className="link-override" to="/admin">
                    <Icon type="file-text" />
                    <span>Страницы</span>
                  </Link>
                </Menu.Item>
                <Menu.Item key="2">
                  <Link className="link-override" to="/admin/slider">
                    <Icon type="switcher" />
                    <span>Слайдеры</span>
                  </Link>
                </Menu.Item>
                <Menu.Item key="3">
                  <Link
                    className="link-override"
                    to="/admin/cards"
                    style={{ display: "none" }}
                  >
                    <Icon type="idcard" />
                    <span>Карточки</span>
                  </Link>
                </Menu.Item>
                <Menu.Item key="4">
                  <Link className="link-override" to="/admin/news">
                    <Icon type="bars" />
                    <span>Новости</span>
                  </Link>
                </Menu.Item>
                <Menu.Item key="5">
                  <Link className="link-override" to="/admin/style">
                    <Icon type="edit" />
                    <span>Оформление</span>
                  </Link>
                </Menu.Item>
                <Menu.Item key="6">
                  <Link className="link-override" to="/admin/settings">
                    <Icon type="like-o" />
                    <span>Соцсети</span>
                  </Link>
                </Menu.Item>
                <Menu.Item key="7">
                  <Link className="link-override" to="/admin/uploader">
                    <Icon type="upload" />
                    <span>Документы</span>
                  </Link>
                </Menu.Item>
                <Menu.Item key="8">
                  <Link className="link-override" to="/admin/storage">
                    <Icon type="cloud" />
                    <span>Хранилище</span>
                  </Link>
                </Menu.Item>
                <Menu.Item key="9">
                  <Link
                    className="link-override"
                    to="/admin/mapeditor"
                    style={{ display: "none" }}
                  >
                    <Icon type="environment-o" />
                    <span>Карта</span>
                  </Link>
                </Menu.Item>
              </Menu>
            )}
          </Affix>
        </Sider>
        <Layout>
          <Header style={{ background: "#fff", padding: 0 }}>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                flexFlow: "column"
              }}
            >
              <div>
                <Icon type="user" />
                {this.state.user.login}
              </div>

              <Link to="/admin/login">выйти</Link>
            </div>
          </Header>
          <Content
            style={{
              margin: "24px 16px",
              padding: 24,
              background: "#fff",
              minHeight: 280
            }}
          >
            <Switch>
              <Route exact path="/admin" component={NavigationManager} />
              <Route path="/admin/edit/page/:pageId" component={Page} />
              <Route path="/admin/create/page/" component={Page} />
              <Route path="/admin/slider" component={SliderManager} />
              <Route exact path="/admin/cards" component={CardsManager} />
              <Route path="/admin/edit/card/:pageId" component={Card} />
              <Route path="/admin/create/card" component={Card} />
              <Route path="/admin/edit/news/:pageId" component={News} />
              <Route path="/admin/create/news" component={News} />
              <Route path="/admin/style" component={CSS} />
              <Route path="/admin/news" component={NewsManager} />
              <Route path="/admin/edit/slider/:sliderId" component={Slider} />
              <Route path="/admin/settings" component={Settings} />
              <Route path="/admin/create/social" component={Social} />
              <Route path="/admin/edit/social/:socialId" component={Social} />
              <Route path="/admin/uploader" component={FileUploadManager} />
              bbb
              <Route path="/admin/storage" component={StorageUploadManager} />
              <Route path="/admin/mapeditor" component={MapManager} />
              <Route path="/admin/create/marker" component={Marker} />
              <Route path="/admin/edit/marker/:markerId" component={Marker} />
            </Switch>
          </Content>
        </Layout>
      </Layout>
    );
  }
}
