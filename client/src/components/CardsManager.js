import React, {Component} from 'react';
import { Affix, Button, Icon, Divider, List, Card, Popconfirm } from 'antd';
import {Link} from 'react-router-dom';

const {Meta} = Card;

export default class CardsManager extends Component {
    state = {
        cards:[],
    };
    componentDidMount(){
        this.getCards();
    }

    getCards(){
        fetch('/api/card')
            .then( res => res.json())
            .then( cards => cards.filter( card => !card.is_news) )
            .then( cards => this.setState({cards: cards}))
            .catch( err => console.log(err));
    }

    deleteCard(itemId){
        fetch(`/api/card/${itemId}`,{
            method: "DELETE"
        })
            .then( () => this.getCards())
            .catch( err => console.log(err) );
    }


    render() {
        const {cards} = this.state;

        return (
            <div className="pages-list-wrapper">
                <h2>Список карточек:</h2>
                <Divider/>
                <Affix offsetTop={0} >
                    <Link to="/admin/create/card" className="pages-list__add-button">
                        <Button type="dashed" style={{width: '60%', marginBottom: 40}}>
                            <Icon type="plus"/> Добавить карточку
                        </Button>
                    </Link>
                </Affix>

                <List
                    itemLayout="horizontal"
                    dataSource={cards}
                    grid={{ gutter: 16, column: 3 }}
                    size="small"
                    renderItem={item => (
                        <List.Item >
                            <Card style={{width: 240}}
                                  cover={<img alt="" src={item.img} width="160" height="160"/>}
                                  actions={[<Link style={{ textDecoration: "none", color: "#a1a1a1"}} to={`/admin/edit/card/${item.id}`}><Icon type="edit"/></Link>,
                                      <Popconfirm title="Подтвердите действие" 
                                      okText="Да" 
                                      cancelText="Нет" 
                                      onCancel={ () => console.log('cancel')} 
                                      onConfirm={this.deleteCard.bind(this, `${item.id}`)}>
                                          <a style={{ textDecoration: "none", color: "#a1a1a1"}}><Icon type="close"/></a></Popconfirm>]}>
                                <Meta style={{textAlign:"center"}}  title={
                                    <Link to={item.link}>{item.title_ru}</Link>
                                }>
                                </Meta>
                            </Card>
                        </List.Item>
                    )}
                />

            </div>
        )
    }
}