import React, { Component } from 'react';
import { Row, Col } from 'antd';

export default class InfoGraphics extends Component {

    render() {
        return (
            this.props.locale == 'ru' ?
                <div className="cards-wrapper main-page-content">
                    <Row>
                        <Col><h1 className="infofig-heading">
                            За <b>20 лет</b> существования <b>ГК ТАЛТЭК</b>
                        </h1></Col>
                    </Row>
                    <Row className="infofig-container">
                        <Col>
                            <div className="infofig">
                                <span>Угольный дивизион</span>
                                <span>ДОБЫТО</span>
                                <span>40 000 000</span>
                                <span>ТОНН УГЛЯ</span>
                            </div>
                        </Col>
                        <Col>
                            <div className="infofig">
                                <span>Кандалакшский МТП</span>
                                <span>ПЕРЕРАБОТАНО</span>
                                <span>10 000 000</span>
                                <span>ТОНН ЭКСПОРТНОГО УГЛЯ</span>
                            </div>
                        </Col>
                        <Col>
                            <div className="infofig">
                                <span>Барнаульский БРЗ</span>
                                <div className="infofig-inner">
                                    <div className="infofig-group">
                                        <span>ПРОИЗВЕДЕНО</span>
                                        <span>7 500</span>
                                        <span>ВАГОНОВ</span>
                                    </div>
                                    <div className="infofig-group">
                                        <span>ОТРЕМОНТИРОВАНО</span>
                                        <span>11 500</span>
                                        <span>ВАГОНОВ</span>
                                    </div>
                                </div>
                            </div>
                        </Col>
                        <Col>
                            <div className="infofig">
                                <span>ТалТЭК<br/>Транс</span>
                                <span>ПЕРЕВЕЗЕНО</span>
                                <span>45 000 000</span>
                                <span>ТОНН ГРУЗОВ</span>
                            </div>
                        </Col>
                    </Row>
                </div>
                :<div className="cards-wrapper main-page-content">
                <Row>
                    <Col><h1 className="infofig-heading">
                        For over than <b>20 years</b> of <b>TALTEK</b> history
                    </h1></Col>
                </Row>
                <Row className="infofig-container">
                    <Col>
                        <div className="infofig">
                            <span>Coal mining</span>
                            <span>EXTRACTED</span>
                            <span>40 000 000</span>
                            <span>TONS OF COAL</span>
                        </div>
                    </Col>
                    <Col>
                        <div className="infofig">
                            <span>Kandalaksha Sea<br/> Trade Port</span>
                            <span>PROCESSED</span>
                            <span>10 000 000</span>
                            <span>TONS OF COAL</span>
                        </div>
                    </Col>
                    <Col>
                        <div className="infofig">
                            <span>Barnaul VRZ</span>
                            <div className="infofig-inner">
                                <div className="infofig-group">
                                    <span>PRODUCED</span>
                                    <span>7 500</span>
                                    <span>TRAIN CARS</span>
                                </div>
                                <div className="infofig-group">
                                    <span>REPAIRED</span>
                                    <span>11 500</span>
                                    <span>TRAIN CARS</span>
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col>
                        <div className="infofig">
                            <span>TalTEK<br/>TRANS</span>
                            <span>DELIVERED</span>
                            <span>45 000 000</span>
                            <span>TONS OF CARGO</span>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

