import React, { Component } from 'react';
import { Row, Col } from 'antd';
import pdf from '../icons8-pdf-40.png';
import './Reports.css';

export default class Reports extends Component {
    state = {
        files: []
    };

    getFiles(){
        fetch('/api/upload/doc')
            .then( res => res.json())
            .then( files => this.setState({files: files.slice(0,4)}))
            .catch( err => console.log(err));
    }

    componentDidMount(){
        this.getFiles();
    }

    render() {
        return (
            <div className="main-page-content document-wrapper">
                <Row>
                    <Col>
                        <h1 className="files" style={{marginLeft: 15}} dangerouslySetInnerHTML={{ __html: this.props.locale == 'ru' ? "Документы" : "Reports"}}></h1>
                    </Col>
                </Row>
                <Row type="flex" align="center" justify="center">
                    { this.state.files.map( file => {
                        return (
                            <a  key={file.name}  href={file.path} download>
                            <Col className="fileinfo">
                                <img className="icon" src={pdf} alt="download"/>
                                <span className="filename">{file.name.slice(0,file.name.length-4)}</span>
                                <span className="filesize">{file.size}</span>
                            </Col>
                            </a>
                        )
                    })}
                </Row>
            </div>
        );
    }
}

