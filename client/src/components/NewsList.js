import React, {Component} from 'react';
import {Col, List} from 'antd';
import {Link} from 'react-router-dom';
import moment from "moment/moment";

export default class NewsList extends Component {
    state = {
        news: [],
        current: 1
    };

    componentDidMount(){
        fetch('/api/card/')
            .then( res => res.json())
            .then( cards => {
                const news = cards.filter( card => card.is_news && card.tags === this.props.type );

                news.sort( (a,b) => {
                    return moment(b.news_date) - moment(a.news_date);
                });

                this.setState({news: news});
            })
            .catch(err => console.log(err));
    }

    render() {
        const {type} = this.props;

        return type === "video" ?
            (
             <div className="page-container">
                 <div className="">
                     <div className="page-element__row" style={{justifyContent:"center"}}>
                     { this.state.news && this.state.news.map( (item, idx) => {
                         return (
                             <Col key={idx} style={{marginTop: 100}}>
                                 <div className="page-element__video" dangerouslySetInnerHTML={{__html: item['text_'+this.props.locale]}}></div>
                             </Col>
                         )
                     })}
                     </div>
                 </div>
             </div>
            )
            :(
            <div className="page-container">
                <div style={{paddingTop:55}}>
                    <div style={{
                        margin: "auto",
                        marginBottom: 30
                    }} className="page-element__row all-news-container">
                        <List
                            pagination={{pageSize: 12}}
                            dataSource={this.state.news}
                            grid={{gutter: 16,xs: 1, sm: 1, md: 2, lg: 2, xl: 3, xxl: 3}}
                            renderItem={ item => (
                                <List.Item>
                                    <Col key={item.id}
                                         className={ item.img === "" ?
                                             "news-item--no-image media-page" :
                                             "news-item--image media-page"}
                                         style={{ backgroundImage: `url(${item.img})`, backgroundSize: "cover"}}>
                                        <span className="news-item__date">{moment(item.news_date).format('LL')}</span>
                                        <h1 className="news-item__heading">{item['title_'+this.props.locale]}</h1>
                                        <Link to={`/media/${item.tags}/${item.id}`} dangerouslySetInnerHTML={{__html: this.props.locale == 'ru' ? "Подробнее &xrarr;" : "Learn more &xrarr;"}}></Link>
                                    </Col>
                                </List.Item>
                            )}/>
                    </div>
                </div>
            </div>
        )
    }
}