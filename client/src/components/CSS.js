import React, {Component} from 'react';
import {Controlled as CodeMirror} from 'react-codemirror2';
import 'codemirror/mode/xml/xml';
import {Affix, Button} from 'antd';

export default class CSS extends Component{

    state = {
        cssFile: null
    };

    componentDidMount(){
        this.getCssFile();
    }

    getCssFile() {
        fetch('/api/css')
            .then( res => res.text() )
            .then( data => {
                this.setState( () => ({cssFile: data}));
            })
    }

    saveCss = (e) => {
        e.preventDefault();
        fetch('/api/css',{
            method: 'POST',
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify(this.state)
        })
        .then(() => this.getCssFile())
        .catch( err => console.log(err));
    };

    render(){
        return (
            <div style={{maxWidth: 960, margin: 'auto', position: "relative"}}>

                <Affix offsetTop={0} offsetBottom={0}>
                    <div  style={{ background: "#FFFFFF", padding: 10, display: "flex", justifyContent: "space-between", alignItems: "baseline"}}>
                        <h3 style={{textAlign: "center"}}>Изменить оформление</h3>
                        <Button onClick={this.saveCss} type="primary" style={{ marginLeft: "auto",  display: "block"}}>Сохранить</Button>
                    </div>

                </Affix>

                <CodeMirror value={this.state.cssFile}
                className="codemirror-editor"
                options={{
                    mode: 'text/css',
                    lineNumbers: true,
                    lineWrapping: true
                }}
                onBeforeChange={(editor, data, value) => {
                    this.setState({cssFile:value});
                }}
                onChange={(editor, data, value) => {
                }}/>

            </div>
        )
    }
}